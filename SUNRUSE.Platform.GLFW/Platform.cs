﻿// <copyright file = "Platform.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>22/06/2014 10:00:56 AM</date>
// <summary>A GLFW-based IPlatform implementation.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel.Composition;

namespace SUNRUSE.Platform.GLFW
{
  /// <summary>An implementation of <see cref="IPlatform"/> using the GLFW library.</summary>
  [Export( typeof( IPlatform ) )]
  public sealed class Platform : IPlatform
  {
    private readonly IInterop Interop;

    private readonly ISettings Settings;

    private readonly ISimulation Simulation;

    void IPlatform.Run()
    {
      var screenWidth = Settings.ScreenWidth;
      if( screenWidth < 1 )
        throw new InvalidOperationException( "ISettings.ScreenWidth should never be less than one" );

      var screenHeight = Settings.ScreenHeight;
      if( screenHeight < 1 )
        throw new InvalidOperationException( "ISettings.ScreenHeight should never be less than one" );

      var windowTitle = Simulation.WindowTitle;
      if( String.IsNullOrWhiteSpace( windowTitle ) )
        throw new InvalidOperationException( "ISimulation.WindowTitle should never be null or whitespace" );

      try
      {
        if( !Interop.Init() )
          throw new ExternalException( "GLFW failed to initialize" );

        if( !Interop.OpenWindow( screenWidth, screenHeight, 8, 8, 8, 0, 24, 0, Settings.Fullscreen ? Constants.FULLSCREEN : Constants.WINDOW ) )
          throw new ExternalException( "GLFW failed to open a context" );

        Interop.SetWindowTitle( windowTitle );

        while( Interop.GetWindowParam( Constants.OPENED ) )
        {
          Simulation.Render( screenWidth, screenHeight );
          Interop.SwapBuffers();
        }
      }
      finally
      {
        Interop.Terminate();
      }
    }

    [ImportingConstructor]
    public Platform( IInterop interop, ISettings settings, ISimulation simulation )
    {
      if( interop == null )
        throw new ArgumentNullException( "interop" );

      if( settings == null )
        throw new ArgumentNullException( "settings" );

      if( simulation == null )
        throw new ArgumentNullException( "simulation" );

      Interop = interop;
      Settings = settings;
      Simulation = simulation;
    }
  }
}
