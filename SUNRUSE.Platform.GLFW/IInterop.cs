﻿// <copyright file = "IInterop.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>22/06/2014 09:59:41 AM</date>
// <summary>An interface for mocking the GLFW library's native interop.</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Platform.GLFW
{
  /// <summary>Wraps the GLFW library for testing purposes.</summary>
  public interface IInterop
  {
    /// <summary>Initializes GLFW.</summary>
    /// <returns><see langword="true"/> if successful, else <see langword="false."/>.</returns>
    Boolean Init();

    /// <summary>Ends GLFW and frees any allocated resources.</summary>
    void Terminate();

    /// <summary>Opens a new OpenGL context using GLFW.</summary>
    /// <remarks>Requires that <see cref="Init"/> be called first.</remarks>
    /// <param name="width">The width of the viewport, in pixels.</param>
    /// <param name="height">The height of the viewport, in pixels.</param>
    /// <param name="redbits">The number of bits to use for the red channel.</param>
    /// <param name="greenbits">The number of bits to use for the green channel.</param>
    /// <param name="bluebits">The number of bits to use for the blue channel.</param>
    /// <param name="alphabits">The number of bits to use for the alpha channel.</param>
    /// <param name="depthbits">The number of bits to use for the depth buffer.</param>
    /// <param name="stencilbits">The number of bits to use for the stencil buffer.</param>
    /// <param name="mode">How to open the window; either <see cref="Constants.WINDOW"/> or <see cref="Constants.FULLSCREEN"/>.</param>
    /// <returns><see langword="true"/> if successful, else <see langword="false."/>.</returns>
    Boolean OpenWindow( int width, int height, int redbits, int greenbits, int bluebits, int alphabits, int depthbits, int stencilbits, int mode );

    /// <summary>Sets the title of the window opened.</summary>
    /// <param name="title">The title to place on the window.</param>
    void SetWindowTitle( String title );

    /// <summary>Sends the created OpenGL buffer to the display and pumps the event queue.</summary>
    void SwapBuffers();

    /// <summary>Retrieves a <see cref="Boolean"/> value from GLFW.</summary>
    /// <param name="param">The value to retrieve; <see cref="Constants.OPENED"/> is supported.</param>
    /// <returns>The value of the parameter specified by <paramref name="param"/>.</returns>
    Boolean GetWindowParam( int param );
  }
}
