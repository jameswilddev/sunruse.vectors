﻿// <copyright file = "IBufferable.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>20/06/2014 10:40:38 PM</date>
// <summary>Interfaces for bufferable types.</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Meshes
{
  /// <summary>Implemented by value types which may be converted to a <see cref="Byte[]"/>.</summary>
  public interface IBufferable
  {
    /// <summary>Writes this <see cref="IBufferable"/> to a <see cref="Byte[]"/>.</summary>
    /// <param name="buffer">The <see cref="Byte[]"/> to write this <see cref="IBufferable"/> to.</param>
    /// <param name="offset">The offset into <paramref name="buffer"/> at which to write this <see cref="IBufferable"/>.</param>
    /// <exception cref="ArgumentNullException">Thrown when <paramref name="buffer"/> is <see langword="null"/>.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown when <paramref name="offset"/> is negative, or </exception>
    void WriteToBuffer( Byte[] buffer, int offset );

    /// <summary>The size of an item in this <see cref="IBufferable"/>.</summary>
    /// <remarks>Must be greater than zero.</remarks>
    int Size { get; }
  }

  /// <summary>Base interface for a component of an <see cref="IBufferable"/>.</summary>
  /// <remarks>Examples would include location or color of a vertex.</remarks>
  public interface IBufferableComponent<T> where T : IBufferableComponent<T>
  {
    /// <summary>The offset from the start of the <see cref="IBufferable"/> at which to find this component, in <see cref="Byte"/>s.</summary>
    /// <remarks>Must be greater than or equal to zero.  Must fit in the <see cref="IBufferable.Size"/> of the <see cref="IBufferable"/> when combined with <see cref="Size"/>.</remarks>
    int Offset { get; }

    /// <summary>The size of this component, in <see cref="Byte"/>s.</summary>
    /// <remarks>Must be greater than zero.  Must fit in the <see cref="IBufferable.Size"/> of the <see cref="IBufferable"/> when combined with <see cref="Offset"/>.</remarks>
    int Size { get; }
  }
}
