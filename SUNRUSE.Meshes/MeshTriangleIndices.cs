﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SUNRUSE.Meshes
{
  /// <summary>A triplet of <see cref="Mesh{TVertex}"/> indices, representing a triangle.</summary>
  /// <remarks>No validation is performed on deserializing or constructing this type as the default constructor cannot be made to do this.</remarks>
  [DataContract( Name = "MeshTriangleIndices", Namespace = "" )]
  public struct MeshTriangleIndices
  {
    /// <summary>The index of the first vertex in this <see cref="MeshTriangleIndices"/>.</summary>
    [DataMember( Name = "A", Order = 0, IsRequired = true )]
    public readonly UInt16 A;

    /// <summary>The index of the second vertex in this <see cref="MeshTriangleIndices"/>.</summary>
    [DataMember( Name = "B", Order = 1, IsRequired = true )]
    public readonly UInt16 B;

    /// <summary>The index of the thid vertex in this <see cref="MeshTriangleIndices"/>.</summary>
    [DataMember( Name = "C", Order = 2, IsRequired = true )]
    public readonly UInt16 C;

    /// <summary>Determines whether this <see cref="MeshTriangleIndices"/> is valid, in that none of its indices refer to the same vertex.</summary>
    /// <remarks>This is automatically checked by the <see cref="Mesh{TVertex}"/> class.</remarks>
    /// <returns><see langword="true"/> when none of this <see cref="MeshTriangleIndices"/>'s indices refer to the same vertex, else <see langword="false"/>.</returns>
    public Boolean CheckIndicesDistinct()
    {
      return A != B && B != C && A != C;
    }

    //todo: Test these methods and serialization

    /// <summary>Determines whether this <see cref="MeshTriangleIndices"/> is valid, in that none of its indices are greater than or equal to a given value.</summary>
    /// <remarks>This is automatically checked by the <see cref="Mesh{TVertex}"/> class against the number of items in the <see cref="Mesh{TVertex}.Vertices"/> collection.</remarks>
    /// <returns><see langword="true"/> when none of this <see cref="MeshTriangleIndices"/>'s indices are greater than <param name="lessThan"/>, else <see langword="false"/>.</returns>
    public Boolean CheckAllLessThan( UInt16 lessThan )
    {
      return A < lessThan && B < lessThan && C < lessThan;
    }

    /// <summary>Determines whether this <see cref="MeshTriangleIndices"/> contains a given value.</summary>
    /// <returns><see langword="true"/> when this <see cref="MeshTriangleIndices"/>'s indices contains <param name="item"/>, else <see langword="false"/>.</returns>
    public Boolean Contains( UInt16 item )
    {
      return A == item || B == item || C == item;
    }

    /// <summary>Determines whether this <see cref="MeshTriangleIndices"/> contains the same items as a given <see cref="MeshTriangleIndices"/>, regardless of order.</summary>
    /// <remarks>This is autmatically checked by the <see cref="Mesh{TVertex}"/> class to ensure that no two <see cref="MeshTriangleIndices"/> in <see cref="Mesh{TVertex}.Indices"/> describe the same triangle.</remarks>
    /// <param name="as">The <see cref="MeshTriangleIndices"/> to compare against.</param>
    /// <returns><see langword="true"/> when each item in <see langword="this"/> exists in <paramref name="as"/>, else <see langword="false"/>.</returns>
    public Boolean ContainSameIndices( MeshTriangleIndices @as )
    {
      return Contains( @as.A ) && Contains( @as.B ) && Contains( @as.C );
    }

    /// <inheritdoc />
    /// <param name="a">The value of <see cref="A"/>.</param>
    /// <param name="b">The value of <see cref="B"/>.</param>
    /// <param name="c">The value of <see cref="C"/>.</param>
    public MeshTriangleIndices( UInt16 a, UInt16 b, UInt16 c )
    {
      A = a;
      B = b;
      C = c;
    }
  }
}
