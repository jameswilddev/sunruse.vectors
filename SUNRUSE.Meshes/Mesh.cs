﻿// <copyright file = "Mesh.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>06/06/2014 12:25:40 AM</date>
// <summary>A Mesh class for containing an indexed triangle soup.</summary>
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace SUNRUSE.Meshes
{
  using Vectors;

  /// <summary>An immutable "soup" of triangles.</summary>
  /// <typeparam name="TVertex">The vertex type on which this <see cref="Mesh{TVertex}"/> is based.</typeparam>
  [DataContract( Name = "Mesh", Namespace = "" )]
  public sealed class Mesh<TVertex>
  {
    [CollectionDataContract( ItemName = "Vertex", Namespace = "" )]
    private sealed class VerticesCollection : List<TVertex>
    {
      internal VerticesCollection( IEnumerable<TVertex> vertices )
        : base( vertices )
      {
      }

      internal VerticesCollection()
      {
      }
    }

    [DataMember( IsRequired = true, Order = 0, Name = "Vertices" )]
    private VerticesCollection _Vertices
    {
      get { return new VerticesCollection( Vertices ); }
      set
      {
        Vertices = value == null ? null : value.AsReadOnly();
      }
    }

    /// <summary>The vertices triangles in <see langword="this"/> <see cref="Mesh{TVertex}"/> are 
    /// made up of.</summary>
    /// <remarks>
    /// Guaranteed to be non-<see langword="null"/>.
    /// Guaranteed to contain at least three items.
    /// </remarks>
    public ReadOnlyCollection<TVertex> Vertices { get; private set; }

    [CollectionDataContract( ItemName = "Triangle", Namespace = "" )]
    private sealed class IndicesCollection : List<MeshTriangleIndices>
    {
      internal IndicesCollection( IEnumerable<MeshTriangleIndices> indices )
        : base( indices )
      {
      }

      internal IndicesCollection()
      {
      }
    }

    [DataMember( IsRequired = true, Order = 1, Name = "Indices" )]
    private IndicesCollection _Indices
    {
      get { return new IndicesCollection( Indices ); }
      set
      {
        Indices = value == null ? null : value.AsReadOnly();
      }
    }

    /// <summary>The indices into <see cref="Vertices"/>, grouped into triplets, creating the
    /// triangles <see langword="this"/> <see cref="Mesh{TVertex}"/> is made up of.</summary>
    /// <remarks>
    /// Guaranteed to be non-<see langword="null"/>.
    /// Guaranteed to contain at least one item.
    /// Guaranteed to not contain indices out of range of <see cref="Vertices"/>.
    /// </remarks>
    public ReadOnlyCollection<MeshTriangleIndices> Indices { get; private set; }

    [OnDeserialized]
    private void OnDeserialized( StreamingContext context )
    {
      if( Vertices == null )
        throw new ArgumentNullException( "Vertices" );

      if( Indices == null )
        throw new ArgumentNullException( "Indices" );

      Validate();
    }

    private void Validate()
    {
      if( !Vertices.Any() )
        throw new ArgumentOutOfRangeException( "vertices", "Cannot be empty" );

      if( Vertices.Count < 3 )
        throw new ArgumentOutOfRangeException( "vertices", "Cannot contain less than three items" );

      if( !Indices.Any() )
        throw new ArgumentOutOfRangeException( "indices", "Cannot be empty" );

      if( !Indices.All( i => i.CheckAllLessThan( ( UInt16 ) Vertices.Count ) ) )
        throw new ArgumentOutOfRangeException( "indices", "Cannot contain out of range items" );
    }

    /// <inheritdoc />
    /// <param name="vertices">The content of <see cref="Vertices"/>.</param>
    /// <param name="indices">The content of <see cref="Indices"/>.</param>
    /// <remarks>For full validation details, see the remarks of <see cref="Vertices"/> and 
    /// <see cref="Indices"/>.</remarks>
    /// <exception cref="ArgumentNullException">Thrown when <paramref name="vertices"/> or
    /// <paramref name="indices"/> is <see langword="null"/>.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown when <paramref name="vertices"/> or
    /// <paramref name="indices"/> fail the validation outlined in <see cref="Vertices"/> and
    /// <see cref="Indices"/>, respectively.</exception> 
    public Mesh( IEnumerable<TVertex> vertices, IEnumerable<MeshTriangleIndices> indices )
    {
      if( vertices == null )
        throw new ArgumentNullException( "vertices" );

      Vertices = vertices.ToList().AsReadOnly();

      if( indices == null )
        throw new ArgumentNullException( "indices" );

      Indices = indices.ToList().AsReadOnly();

      Validate();
    }
  }
}

