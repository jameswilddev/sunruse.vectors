﻿// <copyright file = "VertexInterfaces.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>20/06/2014 13:30:03 PM</date>
// <summary>Interfaces for vertex types.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Meshes
{
  using Vectors;
  using Colors;

  /// <summary>Implemented by vertex types with a location component; allows use in <see cref="T:Byte[]"/> arrays without needing to write complicated code to handle generics.</summary>
  /// <remarks>Implementers should use 32-bit floating point values, and no more than four dimensions.</remarks>
  public interface IVertexLocation : IBufferableComponent<IVertexLocation>
  {
  }

  //todo: this in comments

  /// <summary>Implemented by vertex types with a location component; allows direct access to the component in a type-safe manner.</summary>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="IVertexLocation{TVector}"/> is based.</typeparam>
  public interface IVertexLocation<TVector> : IBufferableComponent<IVertexLocation<TVector>> where TVector : struct, IVector<TVector>
  {
    /// <summary>Gets the location of this vertex.</summary>
    TVector Location { get; }
  }

  /// <summary>Implemented by vertex types with a normal component; allows use in <see cref="T:Byte[]"/> arrays without needing to write complicated code to handle generics.</summary>
  /// <remarks>Implementers should use 32-bit floating point values, and no more than four dimensions.  Some platforms may only support three dimensions.</remarks>
  public interface IVertexNormal : IBufferableComponent<IVertexNormal>
  {
  }

  /// <summary>Implemented by vertex types with a normal component; allows direct access to the component in a type-safe manner.</summary>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="IVertexNormal{TVector}"/> is based.</typeparam>
  public interface IVertexNormal<TVector> : IBufferableComponent<IVertexNormal<TVector>> where TVector : struct, IVector<TVector>
  {
    /// <summary>Gets the normal of this vertex.</summary>
    TVector Normal { get; }
  }

  /// <summary>Implemented by vertex types with a two dimensional texture coordinate component.</summary>
  public interface IVertexTextureCoordinate : IBufferableComponent<IVertexTextureCoordinate>
  {
    /// <summary>Gets the texture coordinate of this vertex.</summary>
    Vector2 TextureCoordinate { get; }
  }

  /// <summary>Implemented by vertex types with a vertex color component; allows use in <see cref="T:Byte[]"/> arrays without needing to write complicated code to handle generics.</summary>
  /// <remarks>Implementers should use an unsigned byte per channel, and no more than four channels.  These channels will be interpreted as RGB and A respectively.</remarks>
  public interface IVertexColor : IBufferableComponent<IVertexColor>
  {
  }

  /// <summary>Implemented by vertex types with a RGB color component.</summary>
  /// <remarks>Should not be implemented as well as <see cref="IVertexColorRGBA"/>.</remarks>
  public interface IVertexColorRGB : IBufferableComponent<IVertexColorRGB>
  {
    /// <summary>Gets the RGB color of this vertex.</summary>
    ColorRGB ColorRGB { get; }
  }

  /// <summary>Implemented by vertex types with a RGBA color component.</summary>
  /// <remarks>Should not be implemented as well as <see cref="IVertexColorRGB"/>.</remarks>
  public interface IVertexColorRGBA : IBufferableComponent<IVertexColorRGBA>
  {
    /// <summary>Gets the RGBA color of this vertex.</summary>
    ColorRGBA ColorRGBA { get; }
  }
}
