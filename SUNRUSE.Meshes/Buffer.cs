﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Meshes
{
  /// <summary>Converts sequences of <typeparamref name="T"/> into fixed-size <see cref="Byte[]"/>s.</summary>
  /// <remarks>This is intended to be used for vertex or index buffers, especially when working with things of a variable number such as particles.</remarks>
  /// <typeparam name="T">The <see cref="Type"/> to buffer.</typeparam>
  public sealed class Buffer<T> where T : struct, IBufferable
  {
    /// <summary>The size of an item in <typeparam name="T"/>.</summary>
    /// <remarks>Retreived at construction.</remarks>
    public readonly int ItemSize;

    /// <summary>The buffer into which <typeparam name="T"/>s are stored.</summary>
    public readonly Byte[] Bytes;

    /// <summary>Called when <see cref="Bytes"/> has been filled.</summary>
    /// <param name="items">The number of items in <see cref="Bytes"/> which have been populated.</param>
    public delegate void FlushDelegate( int items );

    /// <summary>Raised when <see cref="Bytes"/> is filled during <see cref="Process"/>.</summary>
    public event FlushDelegate Flush;

    /// <summary>Converts an <see cref="IEnumerable{T}"/> into buffered <see cref="Byte"/>s in <see cref="Bytes"/>, and raises the <see cref="Flush"/> event when <see cref="Bytes"/> is filled.</summary>
    /// <param name="items">An <see cref="IEnumerable{T}"/> to buffer.</param>
    /// <exception cref="ArgumentNullException">Thrown when <paramref name="items"/> is <see langword="null"/>.</exception>
    public void Process( IEnumerable<T> items )
    {
      if( items == null )
        throw new ArgumentNullException( "items" );

      var flush = Flush;

      int progress = 0;

      foreach( var item in items )
      {
        item.WriteToBuffer( Bytes, progress );

        progress += ItemSize;

        if( progress == Bytes.Length )
        {
          if( flush != null )
            flush( progress / ItemSize );

          progress = 0;
        }
      }

      if( progress != 0 && flush != null )
        flush( progress / ItemSize );
    }

    /// <inheritdoc />
    /// <param name="length">The number of items to store per buffer.</param>
    /// <exception cref="ArgumentOutOfRangeException">Thrown when <paramref name="length"/> is less than one.</exception>
    /// <exception cref="InvalidOperationException">Thrown when <typeparamref name="T"/>.<see cref="IBufferable.Size"/> is less than one.</exception>
    public Buffer( int length )
    {
      if( length < 1 )
        throw new ArgumentOutOfRangeException( "length", "Cannot be less than one" );

      var item = new T();

      ItemSize = item.Size;

      if( ItemSize < 1 )
        throw new InvalidOperationException( "IBufferable.Size should never return less than one" );

      Bytes = new Byte[ ItemSize * length ];
    }
  }
}
