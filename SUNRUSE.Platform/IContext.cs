﻿// <copyright file = "IContext.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 09:08:46 PM</date>
// <summary>An interface for sending instructions to a low level graphics API.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Platform
{
  using Vectors;

  /// <summary>The rendering context through which graphics may be drawn by an <see cref="ISimulation"/>.</summary>
  public interface IContext
  {
    /// <summary>Clears the visible and/or depth buffer.</summary>
    /// <remarks>Bound to the area set by <see cref="Viewport"/>.</remarks>
    /// <param name="color">The color to fill the screen with, if any.</param>
    /// <param name="depth">When <see langword="true"/>, clears the depth buffer too.</param>
    void Clear( Vector3? color, Boolean depth );

    /// <summary>Sets the area of the display to render to, both adding a crop and adjusting normalized device coordinates to match.</summary>
    /// <param name="viewport">
    /// The area, in pixels, to use, where:
    /// <list type="bullet">
    /// <item>0, 0 is the bottom left corner.</item>
    /// <item>(screen width), 0 is the bottom right corner.</item>
    /// <item>0, (screen height) is the top left corner.</item>
    /// <item>(screen width), (screen height) is the top right corner.</item>
    /// </list>
    /// </param>
    void Viewport( Bounds2 viewport );
  }
}
