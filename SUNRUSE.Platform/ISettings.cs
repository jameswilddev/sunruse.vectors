﻿// <copyright file = "ISettings.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 09:12:58 PM</date>
// <summary>An interface for accessing system settings.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace SUNRUSE.Platform
{
  /// <summary>Exposes platform settings.</summary>
  public interface ISettings
  {
    /// <summary>The default width of the screen, in pixels.</summary>
    int ScreenWidth { get; set; }

    /// <summary>The default height of the screen, in pixels.</summary>
    int ScreenHeight { get; set; }

    /// <summary>When <see langword="true"/>, open in full screen mode, else, open a window.</summary>
    Boolean Fullscreen { get; set; }
    
    /// <summary>Saves changes made to properties in this <see cref="ISettings"/> to the backing store.</summary>
    void Save();
  }

  /// <summary>A temporary implementation of <see cref="ISettings"/> which has no backing store.</summary>
  [Export( typeof( ISettings ) )]
  public sealed class Settings : ISettings
  {
    int ISettings.ScreenWidth
    {
      get { return 640; }
      set { throw new NotImplementedException(); }
    }

    int ISettings.ScreenHeight
    {
      get { return 480; }
      set { throw new NotImplementedException(); }
    }

    Boolean ISettings.Fullscreen
    {
      get { return false; }
      set { throw new NotImplementedException(); }
    }

    void ISettings.Save()
    {
      throw new NotImplementedException();
    }
  }
}
