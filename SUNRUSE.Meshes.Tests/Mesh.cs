﻿// <copyright file = "Mesh.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>06/06/2014 11:48:18 AM</date>
// <summary>Tests for the Mesh class.</summary>
#if NUNIT
  using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace SUNRUSE.Meshes.Tests
{
  using Vectors;
  using Vectors.Tests;

  [TestFixture]
  public sealed class MeshTests
  {
    [Test]
    public void Constructor()
    {
      var vertices = new IEnumerableMock<String>();
      vertices.Wrapped = new[] { "Vertex One", "Vertex Two", "Vertex Three" };

      var indices = new IEnumerableMock<MeshTriangleIndices>();
      indices.Wrapped = new[] { new MeshTriangleIndices( 0, 2, 1 ) };

      vertices.ExpectingEnumeration = true;
      indices.ExpectingEnumeration = true;
      var mesh = new Mesh<String>( vertices, indices );
      Assert.IsFalse( vertices.ExpectingEnumeration );
      Assert.IsFalse( indices.ExpectingEnumeration );

      Assert.IsNotNull( mesh.Vertices );
      Assert.AreEqual( 3, mesh.Vertices.Count );
      Assert.AreEqual( "Vertex One", mesh.Vertices[ 0 ] );
      Assert.AreEqual( "Vertex Two", mesh.Vertices[ 1 ] );
      Assert.AreEqual( "Vertex Three", mesh.Vertices[ 2 ] );

      Assert.IsNotNull( mesh.Indices );
      Assert.AreEqual( 1, mesh.Indices.Count );
      Assert.AreEqual( new MeshTriangleIndices( 0, 2, 1 ), mesh.Indices[ 0 ] );
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage =
      @"Value cannot be null.
Parameter name: vertices" )]
    public void ConstructorVerticesNullThrowsException()
    {
      new Mesh<String>( null, null );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot contain less than three items
Parameter name: vertices" )]
    public void ConstructorLessThanThreeVerticesThrowsException()
    {
      new Mesh<String>( new[] { "Vertex One", "Vertex Two" }, new[] { default( MeshTriangleIndices ) } );
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage =
      @"Value cannot be null.
Parameter name: indices" )]
    public void ConstructorIndicesNullThrowsException()
    {
      new Mesh<String>( new[] { "", "", "" }, null );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot be empty
Parameter name: indices" )]
    public void ConstructorIndicesEmptyThrowsException()
    {
      new Mesh<String>( new[] { "", "", "" }, new MeshTriangleIndices[ 0 ] );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot contain out of range items
Parameter name: indices" )]
    public void ConstructorIndicesOutOfRangeThrowsException()
    {
      new Mesh<String>( new[] { "", "", "" }, new[] { new MeshTriangleIndices( 0, 2, 3 ) } );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Mesh<Vector3> ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Mesh<Vector3>( new[] {
						new Vector3( 4.0f, -8.0f, 9.0f ),
						new Vector3( 2.0f, 6.0f, 11.0f ),
						new Vector3( -3.0f, 8.0f, -2.0f ),
					}, new[] { new MeshTriangleIndices( 0, 2, 1 ) } ) );

        Assert.AreEqual( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
		<Vertex>
			<X>-3</X>
			<Y>8</Y>
			<Z>-2</Z>
		</Vertex>
	</Vertices>
	<Indices>
		<Triangle>
			<A>0</A>
			<B>2</B>
			<C>1</C>
		</Triangle>
	</Indices>
</Mesh>", output.GetStringBuilder().ToString() );
      }
    }

    // Fails on Mono; already raised as 19750
    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Mesh<Vector3> ) );
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
		<Vertex>
			<X>-3</X>
			<Y>8</Y>
			<Z>-2</Z>
		</Vertex>
	</Vertices>
	<Indices>
		<Triangle>
			<A>0</A>
			<B>2</B>
			<C>1</C>
		</Triangle>
	</Indices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Mesh<Vector3> ) dataContractSerializer.ReadObject( reader );

        Assert.IsNotNull( deserialized.Vertices );
        Assert.AreEqual( 3, deserialized.Vertices.Count );
        Assert.AreEqual( new Vector3( 4.0f, -8.0f, 9.0f ), deserialized.Vertices[ 0 ] );
        Assert.AreEqual( new Vector3( 2.0f, 6.0f, 11.0f ), deserialized.Vertices[ 1 ] );
        Assert.AreEqual( new Vector3( -3.0f, 8.0f, -2.0f ), deserialized.Vertices[ 2 ] );

        Assert.IsNotNull( deserialized.Indices );
        Assert.AreEqual( 1, deserialized.Indices.Count );
        Assert.AreEqual( new MeshTriangleIndices( 0, 2, 1 ), deserialized.Indices[ 0 ] );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'Indices' from namespace '' is not expected. Expecting element 'Vertices'." )]
    public void DeserializationVerticesMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Mesh<Vector3> ) );
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Indices>
		<Index>0</Index>
		<Index>2</Index>
		<Index>1</Index>
	</Indices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 19 position 3. 'EndElement' 'Mesh' from namespace '' is not expected. Expecting element 'Indices'." )]
    public void DeserializationIndicesMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Mesh<Vector3> ) );
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
		<Vertex>
			<X>-3</X>
			<Y>8</Y>
			<Z>-2</Z>
		</Vertex>
	</Vertices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot contain less than three items
Parameter name: vertices" )]
    public void DeserializationLessThanThreeVerticesThrowsException()
    {
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
	</Vertices>
	<Indices>
		<Index>0</Index>
		<Index>2</Index>
		<Index>1</Index>
	</Indices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
        new DataContractSerializer( typeof( Mesh<Vector3> ) ).ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot be empty
Parameter name: indices" )]
    public void DeserializationIndicesEmptyThrowsException()
    {
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
		<Vertex>
			<X>-3</X>
			<Y>8</Y>
			<Z>-2</Z>
		</Vertex>
	</Vertices>
	<Indices>
	</Indices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
        new DataContractSerializer( typeof( Mesh<Vector3> ) ).ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage =
      @"Cannot contain out of range items
Parameter name: indices" )]
    public void DeserializationIndicesOutOfRangeThrowsException()
    {
      using( var input = new StringReader( @"<Mesh xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Vertices>
		<Vertex>
			<X>4</X>
			<Y>-8</Y>
			<Z>9</Z>
		</Vertex>
		<Vertex>
			<X>2</X>
			<Y>6</Y>
			<Z>11</Z>
		</Vertex>
		<Vertex>
			<X>-3</X>
			<Y>8</Y>
			<Z>-2</Z>
		</Vertex>
	</Vertices>
	<Indices>
		<Triangle>
			<A>0</A>
			<B>1</B>
			<C>4</C>
		</Triangle>
	</Indices>
</Mesh>" ) )
      using( var reader = new XmlTextReader( input ) )
        new DataContractSerializer( typeof( Mesh<Vector3> ) ).ReadObject( reader );
    }
  }
}

