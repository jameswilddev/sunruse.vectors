﻿// <copyright file = "MeshTriangleIndices.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>16/06/2014 07:49:23 AM</date>
// <summary>Tests for the MeshTriangleIndices struct.</summary>
#if NUNIT
  using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace SUNRUSE.Meshes.Tests
{
  using Vectors.Tests;

  [TestFixture]
  public sealed class MeshTriangleIndicesTests
  {
    [Test]
    public void Constructor()
    {
      var meshTriangleIndices = new MeshTriangleIndices( 8, 390, 7862 );

      Assert.AreEqual( 8, meshTriangleIndices.A );
      Assert.AreEqual( 390, meshTriangleIndices.B );
      Assert.AreEqual( 7862, meshTriangleIndices.C );
    }

    [Test]
    public void CheckIndicesDistinct()
    {
      Assert.IsTrue( new MeshTriangleIndices( 8, 390, 7862 ).CheckIndicesDistinct() );
      Assert.IsFalse( new MeshTriangleIndices( 8, 8, 7862 ).CheckIndicesDistinct() );
    }

    [Test]
    public void CheckAllLessThan()
    {
      var meshTriangleIndices = new MeshTriangleIndices( 22498, 297, 129 );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 22497 ) );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 22498 ) );
      Assert.IsTrue( meshTriangleIndices.CheckAllLessThan( 22499 ) );

      meshTriangleIndices = new MeshTriangleIndices( 2098, 38976, 287 );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 38975 ) );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 38976 ) );
      Assert.IsTrue( meshTriangleIndices.CheckAllLessThan( 38977 ) );

      meshTriangleIndices = new MeshTriangleIndices( 322, 8000, 11009 );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 11008 ) );
      Assert.IsFalse( meshTriangleIndices.CheckAllLessThan( 11009 ) );
      Assert.IsTrue( meshTriangleIndices.CheckAllLessThan( 11010 ) );
    }

    [Test]
    public void Contains()
    {
      var meshTriangleIndices = new MeshTriangleIndices( 22390, 2675, 10 );
      Assert.IsTrue( meshTriangleIndices.Contains( 22390 ) );
      Assert.IsTrue( meshTriangleIndices.Contains( 2675 ) );
      Assert.IsTrue( meshTriangleIndices.Contains( 10 ) );
      Assert.IsFalse( meshTriangleIndices.Contains( 22 ) );
      Assert.IsFalse( meshTriangleIndices.Contains( 8390 ) );
      Assert.IsFalse( meshTriangleIndices.Contains( 43209 ) );
    }

    [Test]
    public void ContainSameIndices()
    {
      var meshTriangleIndices = new MeshTriangleIndices( 34, 76, 98 );
      Assert.IsTrue( meshTriangleIndices.ContainSameIndices( meshTriangleIndices ) );
      Assert.IsTrue( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 34, 76, 98 ) ) );
      Assert.IsTrue( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 34, 98, 76 ) ) ); 
      Assert.IsTrue( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 76, 34, 98 ) ) );
      Assert.IsTrue( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 98, 76, 34 ) ) );
      Assert.IsFalse( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 33, 76, 98 ) ) );
      Assert.IsFalse( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 34, 75, 98 ) ) );
      Assert.IsFalse( meshTriangleIndices.ContainSameIndices( new MeshTriangleIndices( 34, 76, 97 ) ) );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( MeshTriangleIndices ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new MeshTriangleIndices( 30, 96, 24 ) );

        Assert.AreEqual( @"<MeshTriangleIndices xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<A>30</A>
	<B>96</B>
	<C>24</C>
</MeshTriangleIndices>", output.GetStringBuilder().ToString() );
      }
    }

    // Fails on Mono; already raised as 19750
    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( MeshTriangleIndices ) );
      using( var input = new StringReader( @"<MeshTriangleIndices xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<A>30</A>
	<B>96</B>
	<C>24</C>
</MeshTriangleIndices>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( MeshTriangleIndices ) dataContractSerializer.ReadObject( reader );
        Assert.AreEqual( new MeshTriangleIndices( 30, 96, 24 ), deserialized );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'B' from namespace '' is not expected. Expecting element 'A'." )]
    public void DeserializationAMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( MeshTriangleIndices ) );
      using( var input = new StringReader( @"<MeshTriangleIndices xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<B>96</B>
	<C>24</C>
</MeshTriangleIndices>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 3 position 3. 'Element' 'C' from namespace '' is not expected. Expecting element 'B'." )]
    public void DeserializationBMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( MeshTriangleIndices ) );
      using( var input = new StringReader( @"<MeshTriangleIndices xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<A>30</A>
	<C>24</C>
</MeshTriangleIndices>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 4 position 3. 'EndElement' 'MeshTriangleIndices' from namespace '' is not expected. Expecting element 'C'." )]
    public void DeserializationCMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( MeshTriangleIndices ) );
      using( var input = new StringReader( @"<MeshTriangleIndices xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<A>30</A>
	<B>96</B>
</MeshTriangleIndices>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }
  }
}

