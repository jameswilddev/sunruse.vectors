﻿// <copyright file = "Buffer.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>20/06/2014 10:32:53 PM</date>
// <summary>Tests for the Buffer class.</summary>
#if NUNIT
using NUnit.Framework;

#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Linq;

namespace SUNRUSE.Meshes.Tests
{
  using Vectors;
  using Vectors.Tests;

  [TestFixture]
  public sealed class BufferTests
  {
    [Test]
    public void Constructor()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var bufferable = new Vertex2() as IBufferable;

      Assert.IsNotNull( buffer.Bytes );
      Assert.AreEqual( 24, buffer.Bytes.Length );
      Assert.AreEqual( 8, buffer.ItemSize );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage = @"Cannot be less than one
Parameter name: length" )]
    public void ConstructorLengthLessThanOneThrowsException()
    {
      new Buffer<Vertex2>( 0 );
    }

    [Test]
    public void ProcessEmptyWithoutFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = Enumerable.Empty<Vertex2>();
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
    }

    [Test]
    public void ProcessFullBufferWithoutFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ), new Vertex2( new Vector2( 7.0f, 4.0f ) ) };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
      Assert.AreEqual( 12.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
      Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer.Bytes, 8 ) );
      Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer.Bytes, 12 ) );
      Assert.AreEqual( 7.0f, BitConverter.ToSingle( buffer.Bytes, 16 ) );
      Assert.AreEqual( 4.0f, BitConverter.ToSingle( buffer.Bytes, 20 ) );
    }

    [Test]
    public void ProcessPartialBufferWithoutFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ) };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
      Assert.AreEqual( 12.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
      Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer.Bytes, 8 ) );
      Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer.Bytes, 12 ) );
    }

    [Test]
    public void ProcessFullBufferAndPartialBufferWithoutFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ), new Vertex2( new Vector2( 7.0f, 4.0f ) ), new Vertex2( new Vector2( 20.0f, -12.0f ) ) };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
    }

    [Test]
    public void ProcessEmptyWithFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = Enumerable.Empty<Vertex2>();
      buffer.Flush += ( int items ) => Assert.Fail();
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
    }

    [Test]
    public void ProcessFullBufferWithFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ), new Vertex2( new Vector2( 7.0f, 4.0f ) ) };
      var hasFlushed = false;
      buffer.Flush += ( int items ) =>
      {
        Assert.IsFalse( hasFlushed );
        hasFlushed = true;

        Assert.AreEqual( 3, items );

        Assert.AreEqual( 12.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
        Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
        Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer.Bytes, 8 ) );
        Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer.Bytes, 12 ) );
        Assert.AreEqual( 7.0f, BitConverter.ToSingle( buffer.Bytes, 16 ) );
        Assert.AreEqual( 4.0f, BitConverter.ToSingle( buffer.Bytes, 20 ) );
      };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
      Assert.IsTrue( hasFlushed );
    }

    [Test]
    public void ProcessPartialBufferWithFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ) };
      var hasFlushed = false;
      buffer.Flush += ( int items ) =>
      {
        Assert.IsFalse( hasFlushed );
        hasFlushed = true;

        Assert.AreEqual( 2, items );

        Assert.AreEqual( 12.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
        Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
        Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer.Bytes, 8 ) );
        Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer.Bytes, 12 ) );
      };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
      Assert.IsTrue( hasFlushed );
    }

    [Test]
    public void ProcessFullBufferAndPartialBufferWithFlushHandler()
    {
      var buffer = new Buffer<Vertex2>( 3 );
      var enumerable = new IEnumerableMock<Vertex2>();
      enumerable.ExpectingEnumeration = true;
      enumerable.Wrapped = new[] { new Vertex2( new Vector2( 12.0f, -4.0f ) ), new Vertex2( new Vector2( 8.0f, 3.0f ) ), new Vertex2( new Vector2( 7.0f, 4.0f ) ), new Vertex2( new Vector2( 5.0f, 4.0f)) };
      var hasFlushedFull = false;
      var hasFlushedPartial = false;
      buffer.Flush += ( int items ) =>
      {
        if( !hasFlushedFull )
        {
          hasFlushedFull = true;

          Assert.AreEqual( 3, items );

          Assert.AreEqual( 12.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
          Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
          Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer.Bytes, 8 ) );
          Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer.Bytes, 12 ) );
          Assert.AreEqual( 7.0f, BitConverter.ToSingle( buffer.Bytes, 16 ) );
          Assert.AreEqual( 4.0f, BitConverter.ToSingle( buffer.Bytes, 20 ) );
        }
        else
        {
          Assert.IsFalse( hasFlushedPartial );
          hasFlushedPartial = true;

          Assert.AreEqual( 1, items );

          Assert.AreEqual( 5.0f, BitConverter.ToSingle( buffer.Bytes, 0 ) );
          Assert.AreEqual( 4.0f, BitConverter.ToSingle( buffer.Bytes, 4 ) );
        }
      };
      buffer.Process( enumerable );
      Assert.IsFalse( enumerable.ExpectingEnumeration );
      Assert.IsTrue( hasFlushedFull );
      Assert.IsTrue( hasFlushedPartial );
    }
  }
}