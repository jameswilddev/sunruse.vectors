﻿// <copyright file = "Vertex.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 11:54:20 AM</date>
// <summary>Tests for the generated Vertex types.</summary>
#if NUNIT
using NUnit.Framework;

#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace SUNRUSE.Meshes.Tests
{
  using Vectors;
  using Colors;

  [TestFixture]
  public sealed class VertexTests
  {
    [Test]
    public void ConstructorRGBA()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGBA( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGBA( 87, 72, 224, 96 ) );
      Assert.AreEqual( new Vector3( 3.0f, -8.0f, -4.0f ), vertex.Location );
      Assert.AreEqual( new Vector3( 8.0f, 9.0f, 2.0f ), vertex.Normal );
      Assert.AreEqual( new Vector2( -4.0f, -7.0f ), vertex.TextureCoordinate );
      Assert.AreEqual( new ColorRGBA( 87, 72, 224, 96 ), vertex.ColorRGBA );

      var vertexLocation = vertex as IVertexLocation<Vector3>;
      Assert.AreEqual( new Vector3( 3.0f, -8.0f, -4.0f ), vertexLocation.Location );
      Assert.AreEqual( 0, vertexLocation.Offset );
      Assert.AreEqual( 12, vertexLocation.Size );
      var vertexLocationGeneric = vertex as IVertexLocation;
      Assert.AreEqual( 0, vertexLocationGeneric.Offset );
      Assert.AreEqual( 12, vertexLocationGeneric.Size );

      var vertexNormal = vertex as IVertexNormal<Vector3>;
      Assert.AreEqual( new Vector3( 8.0f, 9.0f, 2.0f ), vertexNormal.Normal );
      Assert.AreEqual( 12, vertexNormal.Offset );
      Assert.AreEqual( 12, vertexNormal.Size );
      var vertexNormalGeneric = vertex as IVertexNormal;
      Assert.AreEqual( 12, vertexNormalGeneric.Offset );
      Assert.AreEqual( 12, vertexNormalGeneric.Size );

      var vertexTextureCoordinate = vertex as IVertexTextureCoordinate;
      Assert.AreEqual( new Vector2( -4.0f, -7.0f ), vertexTextureCoordinate.TextureCoordinate );
      Assert.AreEqual( 24, vertexTextureCoordinate.Offset );
      Assert.AreEqual( 8, vertexTextureCoordinate.Size );

      var vertexColorRGBA = vertex as IVertexColorRGBA;
      Assert.AreEqual( new ColorRGBA( 87, 72, 224, 96 ), vertexColorRGBA.ColorRGBA );
      Assert.AreEqual( 32, vertexColorRGBA.Offset );
      Assert.AreEqual( 4, vertexColorRGBA.Size );
      var vertexColorRGBAGeneric = vertex as IVertexColor;
      Assert.AreEqual( 32, vertexColorRGBAGeneric.Offset );
      Assert.AreEqual( 4, vertexColorRGBAGeneric.Size );

      var vertexBufferable = vertex as IBufferable;
      Assert.AreEqual( 36, vertexBufferable.Size );
    }

    [Test]
    public void ConstructorRGB()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      Assert.AreEqual( new Vector3( 3.0f, -8.0f, -4.0f ), vertex.Location );
      Assert.AreEqual( new Vector3( 8.0f, 9.0f, 2.0f ), vertex.Normal );
      Assert.AreEqual( new Vector2( -4.0f, -7.0f ), vertex.TextureCoordinate );
      Assert.AreEqual( new ColorRGB( 98, 26, 197 ), vertex.ColorRGB );

      var vertexLocation = vertex as IVertexLocation<Vector3>;
      Assert.AreEqual( new Vector3( 3.0f, -8.0f, -4.0f ), vertexLocation.Location );
      Assert.AreEqual( 0, vertexLocation.Offset );
      Assert.AreEqual( 12, vertexLocation.Size );
      var vertexLocationGeneric = vertex as IVertexLocation;
      Assert.AreEqual( 0, vertexLocationGeneric.Offset );
      Assert.AreEqual( 12, vertexLocationGeneric.Size );

      var vertexNormal = vertex as IVertexNormal<Vector3>;
      Assert.AreEqual( new Vector3( 8.0f, 9.0f, 2.0f ), vertexNormal.Normal );
      Assert.AreEqual( 12, vertexNormal.Offset );
      Assert.AreEqual( 12, vertexNormal.Size );
      var vertexNormalGeneric = vertex as IVertexNormal;
      Assert.AreEqual( 12, vertexNormalGeneric.Offset );
      Assert.AreEqual( 12, vertexNormalGeneric.Size );

      var vertexTextureCoordinate = vertex as IVertexTextureCoordinate;
      Assert.AreEqual( new Vector2( -4.0f, -7.0f ), vertexTextureCoordinate.TextureCoordinate );
      Assert.AreEqual( 24, vertexTextureCoordinate.Offset );
      Assert.AreEqual( 8, vertexTextureCoordinate.Size );

      var vertexColorRGB = vertex as IVertexColorRGB;
      Assert.AreEqual( new ColorRGB( 98, 26, 197 ), vertexColorRGB.ColorRGB );
      Assert.AreEqual( 32, vertexColorRGB.Offset );
      Assert.AreEqual( 3, vertexColorRGB.Size );
      var vertexColorRGBGeneric = vertex as IVertexColor;
      Assert.AreEqual( 32, vertexColorRGBGeneric.Offset );
      Assert.AreEqual( 3, vertexColorRGBGeneric.Size );

      var vertexBufferable = vertex as IBufferable;
      Assert.AreEqual( 35, vertexBufferable.Size );
    }

    [Test]
    public void WriteToBufferRGB()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      var buffer = new Byte[ 72 ];

      for(int i = 0; i < buffer.Length; i++)
        buffer[i] = 160;

      var vertexBufferable = vertex as IBufferable;

      vertexBufferable.WriteToBuffer( buffer, 20 );

      for( int i = 0; i < 20; i++ )
        Assert.AreEqual( 160, buffer[ i ] );

      Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer, 20 ) );
      Assert.AreEqual( -8.0f, BitConverter.ToSingle( buffer, 24 ) );
      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer, 28 ) );

      Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer, 32 ) );
      Assert.AreEqual( 9.0f, BitConverter.ToSingle( buffer, 36 ) );
      Assert.AreEqual( 2.0f, BitConverter.ToSingle( buffer, 40 ) );

      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer, 44 ) );
      Assert.AreEqual( -7.0f, BitConverter.ToSingle( buffer, 48 ) );

      Assert.AreEqual( 98, buffer[ 52 ] );
      Assert.AreEqual( 26, buffer[ 53 ] );
      Assert.AreEqual( 197, buffer[ 54 ] );

      for( int i = 55; i < 72; i++ )
        Assert.AreEqual( 160, buffer[ i ] );
    }

    [Test]
    public void WriteToBufferRGBA()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGBA( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGBA( 87, 72, 224, 96 ) );
      var buffer = new Byte[ 72 ];

      for( int i = 0; i < buffer.Length; i++ )
        buffer[ i ] = 160;

      var vertexBufferable = vertex as IBufferable;

      vertexBufferable.WriteToBuffer( buffer, 20 );

      for( int i = 0; i < 20; i++ )
        Assert.AreEqual( 160, buffer[ i ] );

      Assert.AreEqual( 3.0f, BitConverter.ToSingle( buffer, 20 ) );
      Assert.AreEqual( -8.0f, BitConverter.ToSingle( buffer, 24 ) );
      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer, 28 ) );

      Assert.AreEqual( 8.0f, BitConverter.ToSingle( buffer, 32 ) );
      Assert.AreEqual( 9.0f, BitConverter.ToSingle( buffer, 36 ) );
      Assert.AreEqual( 2.0f, BitConverter.ToSingle( buffer, 40 ) );

      Assert.AreEqual( -4.0f, BitConverter.ToSingle( buffer, 44 ) );
      Assert.AreEqual( -7.0f, BitConverter.ToSingle( buffer, 48 ) );

      Assert.AreEqual( 87, buffer[ 52 ] );
      Assert.AreEqual( 72, buffer[ 53 ] );
      Assert.AreEqual( 224, buffer[ 54 ] );
      Assert.AreEqual( 96, buffer[ 55 ] );

      for( int i = 56; i < 72; i++ )
        Assert.AreEqual( 160, buffer[ i ] );
    }

    [Test]
    [ExpectedException( typeof( ArgumentOutOfRangeException ), ExpectedMessage = @"Cannot be less than zero
Parameter name: offset" )]
    public void WriteToBufferOffsetLessThanZero()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      var buffer = new Byte[ 72 ];
      var vertexBufferable = vertex as IBufferable;
      vertexBufferable.WriteToBuffer( buffer, -1 );
    }

    [Test]
    [ExpectedException( typeof( ArgumentException ), ExpectedMessage = @"buffer is not large enough to contain this type at the given offset" )]
    public void WriteToBufferOffsetTooLarge()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      var buffer = new Byte[ 72 ];
      var vertexBufferable = vertex as IBufferable;
      vertexBufferable.WriteToBuffer( buffer, 38 );
    }

    [Test]
    public void WriteToBufferOffsetOnLowerBound()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      var buffer = new Byte[ 72 ];
      var vertexBufferable = vertex as IBufferable;
      vertexBufferable.WriteToBuffer( buffer, 0 );
    }

    [Test]
    public void WriteToBufferOffsetOnUpperBound()
    {
      var vertex = new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) );
      var buffer = new Byte[ 72 ];
      var vertexBufferable = vertex as IBufferable;
      vertexBufferable.WriteToBuffer( buffer, 37 );
    }

    [Test]
    public void SerializationRGBA()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vertex3NormalTextureCoordinateColorRGBA ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Vertex3NormalTextureCoordinateColorRGBA( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGBA( 87, 72, 224, 96 ) ) );

        Assert.AreEqual( @"<Vertex3NormalTextureCoordinateColorRGBA xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Location>
		<X>3</X>
		<Y>-8</Y>
		<Z>-4</Z>
	</Location>
	<Normal>
		<X>8</X>
		<Y>9</Y>
		<Z>2</Z>
	</Normal>
	<TextureCoordinate>
		<X>-4</X>
		<Y>-7</Y>
	</TextureCoordinate>
	<ColorRGBA>
		<Red>87</Red>
		<Green>72</Green>
		<Blue>224</Blue>
		<Alpha>96</Alpha>
	</ColorRGBA>
</Vertex3NormalTextureCoordinateColorRGBA>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void DeserializationRGBA()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vertex3NormalTextureCoordinateColorRGBA ) );
      using( var input = new StringReader( @"<Vertex3NormalTextureCoordinateColorRGBA xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Location>
		<X>3</X>
		<Y>-8</Y>
		<Z>-4</Z>
	</Location>
	<Normal>
		<X>8</X>
		<Y>9</Y>
		<Z>2</Z>
	</Normal>
	<TextureCoordinate>
		<X>-4</X>
		<Y>-7</Y>
	</TextureCoordinate>
	<ColorRGBA>
		<Red>87</Red>
		<Green>72</Green>
		<Blue>224</Blue>
		<Alpha>96</Alpha>
	</ColorRGBA>
</Vertex3NormalTextureCoordinateColorRGBA>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Vertex3NormalTextureCoordinateColorRGBA ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new Vertex3NormalTextureCoordinateColorRGBA( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGBA( 87, 72, 224, 96 ) ), deserialized );
      }
    }

    [Test]
    public void SerializationRGB()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vertex3NormalTextureCoordinateColorRGB ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) ) );

        Assert.AreEqual( @"<Vertex3NormalTextureCoordinateColorRGB xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Location>
		<X>3</X>
		<Y>-8</Y>
		<Z>-4</Z>
	</Location>
	<Normal>
		<X>8</X>
		<Y>9</Y>
		<Z>2</Z>
	</Normal>
	<TextureCoordinate>
		<X>-4</X>
		<Y>-7</Y>
	</TextureCoordinate>
	<ColorRGB>
		<Red>98</Red>
		<Green>26</Green>
		<Blue>197</Blue>
	</ColorRGB>
</Vertex3NormalTextureCoordinateColorRGB>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void DeserializationRGB()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vertex3NormalTextureCoordinateColorRGB ) );
      using( var input = new StringReader( @"<Vertex3NormalTextureCoordinateColorRGB xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Location>
		<X>3</X>
		<Y>-8</Y>
		<Z>-4</Z>
	</Location>
	<Normal>
		<X>8</X>
		<Y>9</Y>
		<Z>2</Z>
	</Normal>
	<TextureCoordinate>
		<X>-4</X>
		<Y>-7</Y>
	</TextureCoordinate>
	<ColorRGB>
		<Red>98</Red>
		<Green>26</Green>
		<Blue>197</Blue>
	</ColorRGB>
</Vertex3NormalTextureCoordinateColorRGB>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Vertex3NormalTextureCoordinateColorRGB ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) ), deserialized );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'Normal' from namespace '' is not expected. Expecting element 'Location'." )]
    public void DeserializationComponentMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vertex3NormalTextureCoordinateColorRGBA ) );
      using( var input = new StringReader( @"<Vertex3NormalTextureCoordinateColorRGBA xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Normal>
		<X>8</X>
		<Y>9</Y>
		<Z>2</Z>
	</Normal>
	<TextureCoordinate>
		<X>-4</X>
		<Y>-7</Y>
	</TextureCoordinate>
	<ColorRGBA>
		<Red>87</Red>
		<Green>72</Green>
		<Blue>224</Blue>
		<Alpha>96</Alpha>
	</ColorRGBA>
</Vertex3NormalTextureCoordinateColorRGBA>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    public new void ToString()
    {
      Assert.AreEqual( "Vertex3NormalTextureCoordinateColorRGB{ Location: Vector3{ X: 3, Y: -8, Z: -4 }, Normal: Vector3{ X: 8, Y: 9, Z: 2 }, TextureCoordinate: Vector2{ X: -4, Y: -7 }, ColorRGB: ColorRGB{ Red: 98, Green: 26, Blue: 197 } }", new Vertex3NormalTextureCoordinateColorRGB( new Vector3( 3.0f, -8.0f, -4.0f ), new Vector3( 8.0f, 9.0f, 2.0f ), new Vector2( -4.0f, -7.0f ), new ColorRGB( 98, 26, 197 ) ).ToString() );
    }
  }
}