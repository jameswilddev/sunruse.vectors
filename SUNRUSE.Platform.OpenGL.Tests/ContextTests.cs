﻿// <copyright file = "ContextTests.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 18:16:28 AM</date>
// <summary>Tests for the OpenGL implementation of Context.</summary>
#if NUNIT
  using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace SUNRUSE.Platform.OpenGL.Tests
{
  using Vectors;
  using Vectors.Tests;

  [TestFixture]
  public sealed class ContextTests
  {
    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage = @"Value cannot be null.
Parameter name: interop" )]
    public void ConstructorInteropNullThrowsException()
    {
      new Context( null );
    }

    public sealed class InteropMock : IInterop
    {
      public Action<int, int, int, int> ValidateViewport = null;

      void IInterop.Viewport( int x, int y, int width, int height )
      {
        ValidateViewport( x, y, width, height );
      }

      public Action<int, int, int, int> ValidateScissor = null;

      void IInterop.Scissor( int x, int y, int width, int height )
      {
        ValidateScissor( x, y, width, height );
      }

      public Action<int> ValidateClear = null;

      void IInterop.Clear( int mask )
      {
        ValidateClear( mask );
      }

      public Action<Single, Single, Single, Single> ValidateClearColor = null;

      void IInterop.ClearColor( Single red, Single green, Single blue, Single alpha )
      {
        ValidateClearColor( red, green, blue, alpha );
      }

      public Action<int> ValidateEnable = null;

      void IInterop.Enable( int cap )
      {
        ValidateEnable( cap );
      }

      public Action<int> ValidateDisable = null;

      void IInterop.Disable( int cap )
      {
        ValidateDisable( cap );
      }
    }

    [Test]
    public void ClearNeitherVisibleNorDepthBuffers()
    {
      var interop = new InteropMock();
      var context = new Context( interop ) as IContext;

      context.Clear( ( Vector3? ) null, false );
    }

    [Test]
    public void ClearVisibleBufferOnly()
    {
      var interop = new InteropMock();
      var context = new Context( interop ) as IContext;

      var setClearColor = false;
      var enableScissor = false;
      var clear = false;
      var disableScissor = false;

      interop.ValidateClearColor = ( Single red, Single green, Single blue, Single alpha ) =>
      {
        Assert.AreEqual( 4.0f, red );
        Assert.AreEqual( -3.0f, green );
        Assert.AreEqual( 2.0f, blue );
        Assert.AreEqual( 0.0f, alpha );

        Assert.IsFalse( setClearColor );
        setClearColor = true;
      };

      interop.ValidateEnable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( enableScissor );
        enableScissor = true;
      };

      interop.ValidateClear = ( int mask ) =>
      {
        Assert.AreEqual( Constants.COLOR_BUFFER_BIT, mask );
        Assert.IsTrue( enableScissor );
        Assert.IsTrue( setClearColor );
        Assert.IsFalse( clear );
        clear = true;
      };

      interop.ValidateDisable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( disableScissor );
        disableScissor = true;
      };

      context.Clear( new Vector3( 4.0f, -3.0f, 2.0f ), false );

      Assert.IsTrue( setClearColor );
      Assert.IsTrue( enableScissor );
      Assert.IsTrue( clear );
      Assert.IsTrue( disableScissor );
    }

    [Test]
    public void ClearDepthBufferOnly()
    {
      var interop = new InteropMock();
      var context = new Context( interop ) as IContext;

      var enableScissor = false;
      var clear = false;
      var disableScissor = false;

      interop.ValidateEnable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( enableScissor );
        enableScissor = true;
      };

      interop.ValidateClear = ( int mask ) =>
      {
        Assert.AreEqual( Constants.DEPTH_BUFFER_BIT, mask );
        Assert.IsTrue( enableScissor );
        Assert.IsFalse( clear );
        clear = true;
      };

      interop.ValidateDisable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( disableScissor );
        disableScissor = true;
      };

      context.Clear( ( Vector3? ) null, true );

      Assert.IsTrue( enableScissor );
      Assert.IsTrue( clear );
      Assert.IsTrue( disableScissor );
    }

    [Test]
    public void ClearBothVisibleAndDepthBuffers()
    {
      var interop = new InteropMock();
      var context = new Context( interop ) as IContext;

      var setClearColor = false;
      var enableScissor = false;
      var clear = false;
      var disableScissor = false;

      interop.ValidateClearColor = ( Single red, Single green, Single blue, Single alpha ) =>
      {
        Assert.AreEqual( 4.0f, red );
        Assert.AreEqual( -3.0f, green );
        Assert.AreEqual( 2.0f, blue );
        Assert.AreEqual( 0.0f, alpha );

        Assert.IsFalse( setClearColor );
        setClearColor = true;
      };

      interop.ValidateEnable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( enableScissor );
        enableScissor = true;
      };

      interop.ValidateClear = ( int mask ) =>
      {
        Assert.AreEqual( Constants.COLOR_BUFFER_BIT | Constants.DEPTH_BUFFER_BIT, mask );
        Assert.IsTrue( enableScissor );
        Assert.IsTrue( setClearColor );
        Assert.IsFalse( clear );
        clear = true;
      };

      interop.ValidateDisable = ( int cap ) =>
      {
        Assert.AreEqual( Constants.SCISSOR_TEST, cap );
        Assert.IsFalse( disableScissor );
        disableScissor = true;
      };

      context.Clear( new Vector3( 4.0f, -3.0f, 2.0f ), true );

      Assert.IsTrue( setClearColor );
      Assert.IsTrue( enableScissor );
      Assert.IsTrue( clear );
      Assert.IsTrue( disableScissor );
    }

    [Test]
    public void Viewport()
    {
      var interop = new InteropMock();
      var context = new Context( interop ) as IContext;

      var setViewport = false;
      var setScissor = false;

      interop.ValidateViewport = ( int x, int y, int width, int height ) =>
      {
        Assert.AreEqual( 20, x );
        Assert.AreEqual( 60, y );
        Assert.AreEqual( 280, width );
        Assert.AreEqual( 340, height );

        Assert.IsFalse( setViewport );
        setViewport = true;
      };

      interop.ValidateScissor = ( int x, int y, int width, int height ) =>
      {
        Assert.AreEqual( 20, x );
        Assert.AreEqual( 60, y );
        Assert.AreEqual( 280, width );
        Assert.AreEqual( 340, height );

        Assert.IsFalse( setScissor );
        setScissor = true;
      };

      context.Viewport( new Bounds2( new Vector2( 20.0f, 60.0f ), new Vector2( 300.0f, 400.0f ) ) );

      Assert.IsTrue( setViewport );
      Assert.IsTrue( setScissor );
    }
  }
}

