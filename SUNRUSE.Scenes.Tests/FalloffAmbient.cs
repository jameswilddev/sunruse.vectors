﻿// <copyright file = "FalloffAmbient.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 12:48:37 AM</date>
// <summary>Tests for the FalloffAmbient class.</summary>
#if NUNIT
  using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace SUNRUSE.Scenes.Tests
{
  using SUNRUSE.Vectors;
  using SUNRUSE.Vectors.Tests;

  [TestFixture]
  public sealed class FalloffAmbientTests
  {
    [Test]
    public void Constructor()
    {
      var falloff = new FalloffAmbient<Vector3>( new Vector3( 6.0f, -7.0f, 8.0f ) );
      Assert.AreEqual( new Vector3( 6.0f, -7.0f, 8.0f ), falloff.Vector );
    }

    [Test]
    public void SampleVector()
    {
      var falloff = new FalloffAmbient<Vector3>( new Vector3( 3.0f, 4.0f, 7.0f ) ) as IFalloff<Vector3>;
      Assert.AreEqual( new Vector3( 3.0f, 4.0f, 7.0f ), falloff.Sample( new Vector3( 2.0f, -4.0f, -7.0f ) ) );
      Assert.AreEqual( new Vector3( 3.0f, 4.0f, 7.0f ), falloff.Sample( new Vector3( -9.0f, 2.0f, 1.0f ) ) );
    }

    [Test]
    public void SampleNormalMagnitude()
    {
      var falloff = new FalloffAmbient<Vector3>( new Vector3( 3.0f, 4.0f, 7.0f ) ) as IFalloff<Vector3>;

      Vector3 normal;
      Single magnitude;
      falloff.Sample( new Vector3( 2.0f, -4.0f, -7.0f ), out normal, out magnitude );
      VectorCheck.AssertNearEqual( new Vector3(0.348743f, 0.464991f, 0.813733f), normal );
      Assert.AreEqual( 8.60233f, magnitude, 0.001f );
      falloff.Sample( new Vector3( -9.0f, 2.0f, 1.0f ), out normal, out magnitude );
      VectorCheck.AssertNearEqual( new Vector3( 0.348743f, 0.464991f, 0.813733f ), normal );
      Assert.AreEqual( 8.60233f, magnitude, 0.001f );
    }

    [Test]
    public void SampleMagnitude()
    {
      var falloff = new FalloffAmbient<Vector3>( new Vector3( 3.0f, 4.0f, 7.0f ) ) as IFalloff<Vector3>;
      Assert.AreEqual( 8.60233f, falloff.SampleMagnitude( new Vector3( 2.0f, -4.0f, -7.0f ) ), 0.001f );
      Assert.AreEqual( 8.60233f, falloff.SampleMagnitude( new Vector3( -9.0f, 2.0f, 1.0f ) ), 0.001f );
    }
  }
}

