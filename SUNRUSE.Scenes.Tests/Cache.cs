﻿// <copyright file = "Cache.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 09:07:43 PM</date>
// <summary>Tests for the Cache class.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SUNRUSE.Scenes.Tests
{
  using Vectors;
  using Vectors.Tests;

  [TestFixture]
  public sealed class CacheTests
  {
    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage =
      @"Value cannot be null.
Parameter name: entities" )]
    public void NullThrowsException()
    {
      new Cache( null );
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage =
      @"Cannot contain null
Parameter name: entities" )]
    public void ContainsNullThrowsException()
    {
      new Cache( new[] { "Test", null } );
    }

    [Test]
    public void Empty()
    {
      var cache = new Cache( Enumerable.Empty<Object>() );
      var enumerable = cache.GetEntities<Object>();
      Assert.IsNotNull( enumerable );
      Assert.IsFalse( enumerable.Any() );
    }

    [Test]
    public void OneType()
    {
      var cache = new Cache( new[] { "Test" } );
      var enumerable = cache.GetEntities<String>();
      Assert.IsNotNull( enumerable );
      Assert.AreEqual( 1, enumerable.Count() );
      Assert.AreEqual( "Test", enumerable.First() );
    }

    [Test]
    public void MultipleTypes()
    {
      var cache = new Cache( new Object[] { 4, "Test", 1 } );

      var intEnumerable = cache.GetEntities<int>();
      Assert.IsNotNull( intEnumerable );
      Assert.AreEqual( 2, intEnumerable.Count() );
      Assert.AreEqual( 4, intEnumerable.First() );
      Assert.AreEqual( 1, intEnumerable.Skip( 1 ).First() );

      var stringEnumerable = cache.GetEntities<String>();
      Assert.IsNotNull( stringEnumerable );
      Assert.AreEqual( 1, stringEnumerable.Count() );
      Assert.AreEqual( "Test", stringEnumerable.First() );
    }

    [Test]
    public void AccessEmptyType()
    {
      var cache = new Cache( new[] { "Test" } );
      var enumerable = cache.GetEntities<int>();
      Assert.IsNotNull( enumerable );
      Assert.IsFalse( enumerable.Any() );
    }
  }
}

