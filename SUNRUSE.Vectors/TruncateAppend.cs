﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Vectors
{
	/// <summary>An extension of <see cref="IVector{TVector}"/> which allows the vector to be 
	/// truncated.  (removal of the last component)</summary>
	/// <typeparam name="TVector">The implementing type.</typeparam>
	/// <typeparam name="TTruncated">The <see cref="IVector{TVector}"/> which this 
	/// <see cref="IVectorTruncatable{TVector, TTruncated}"/> truncates to.</typeparam>
	public interface IVectorTruncatable<TVector, TTruncated> : IVector<TVector>
    where TVector : struct, IVectorTruncatable<TVector, TTruncated>
    where TTruncated : struct, IVectorAppendable<TTruncated, TVector>
	{
		/// <summary>Truncates this <typeparamref name="TVector"/> to a 
		/// <typeparamref name="TTruncated"/>, removing the last component in the process.</summary>
		TTruncated Truncate { get; }
	}

	/// <summary>An extension of <see cref="IVector{TVector}"/> which allows the vector to be 
	/// appended.  (appending of a single component to the end)</summary>
	/// <typeparam name="TVector">The implementing type.</typeparam>
	/// <typeparam name="TAppended">The <see cref="IVector{TVector}"/> which this 
	/// <see cref="IVectorAppendable{TVector, TAppended}"/> appends to.</typeparam>
	public interface IVectorAppendable<TVector, TAppended> : IVector<TVector>
    where TVector : struct, IVectorAppendable<TVector, TAppended>
    where TAppended : struct, IVectorTruncatable<TAppended, TVector>
	{
		/// <summary>Truncates this <typeparamref name="TVector"/> to a 
		/// <typeparamref name="TAppended"/>, appending a component to the end in the process.</summary>
		/// <param name="append">The <see cref="Single"/> to append to <see langword="this"/>.</param>
		TAppended Append( Single append );
	}
}
