﻿// <copyright file = "ITransform.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 07:43:44 AM</date>
// <summary>Interfaces for interacting with transformations of vectors in a 
//	generic manner.</summary>
using System;

namespace SUNRUSE.Vectors
{
	/// <summary>A transform which may be applied to a 
	/// <typeparamref name="TVector"/>.</summary>
	/// <typeparam name="TVector">The <see cref="IVector{TVector}"/> type this 
	/// <see cref="ITransform{TVector}"/> 
	/// transforms.</typeparam>
	/// <remarks>All <see cref="ITransform{TVector}"/>s must be reversible.
	/// </remarks>
	public interface ITransform<TVector> where TVector : struct, IVector<TVector>
	{
		/// <summary>Applies this <see cref="ITransform{TVector}"/> to a given 
		/// <typeparamref name="TVector"/>.</summary>
		/// <param name="vector">The <typeparamref name="TVector"/> to transform.</param>
		/// <returns><paramref name="vector"/> transformed by this <see cref="ITransform{TVector}"/>.
		/// </returns>
		TVector Transform( TVector vector );

		/// <summary>Applies the inverse of this <see cref="ITransform{TVector}"/> to a given 
		/// <typeparamref name="TVector"/>.</summary>
		/// <param name="vector">The <typeparamref name="TVector"/> to untransform.</param>
		/// <returns><paramref name="vector"/> transformed by the inverse of this 
		/// <see cref="ITransform{TVector}"/>.</returns>
		/// <remarks>This should reverse the operation performed by <see cref="Transform"/>.</remarks>
		TVector Untransform( TVector vector );
	}
}

