﻿// <copyright file = "Triangle.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 17:35:44 PM</date>
// <summary>A struct describing a triangle.</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Vectors
{
  //todo: serialization
  /// <summary>Encapsulates the three vertices of a triangle and allows testing of whether a point falls on that triangle.</summary>
  /// <remarks>Optimized algorithm based on <a>http://www.blackpawn.com/texts/pointinpoly/</a>.</remarks>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="Triangle{TVector}"/> is based.</typeparam>
  public struct Triangle<TVector> where TVector : struct, IVector<TVector>
  {
    /// <summary>The location of the first vertex.</summary>
    public readonly TVector A;

    /// <summary>The location of the second vertex.</summary>
    public readonly TVector B;

    /// <summary>The location of the third vertex.</summary>
    public readonly TVector C;

    /// <summary>The difference to <see cref="B"/> from <see cref="A"/>.</summary>
    /// <remarks>Used when computing barycentric coordinates, and by extension, when determining whether a point is within this <see cref="Triangle{TVector}"/></remarks>
    public readonly TVector BA;

    /// <summary>The difference to <see cref="C"/> from <see cref="A"/>.</summary>
    /// <remarks>Used when computing barycentric coordinates, and by extension, when determining whether a point is within this <see cref="Triangle{TVector}"/></remarks>
    public readonly TVector CA;

    /// <summary>The square of the distance from <see cref="B"/> to <see cref="A"/>.</summary>
    /// <remarks>Used when computing barycentric coordinates, and by extension, when determining whether a point is within this <see cref="Triangle{TVector}"/></remarks>
    public readonly Single BAMagnitudeSquared;

    /// <summary>The square of the distance from <see cref="C"/> to <see cref="A"/>.</summary>
    /// <remarks>Used when computing barycentric coordinates, and by extension, when determining whether a point is within this <see cref="Triangle{TVector}"/></remarks>
    public readonly Single CAMagnitudeSquared;

    /// <summary>The dot product of <see cref="CA"/> and <see cref="BA"/>.</summary>
    /// <remarks>Used when computing barycentric coordinates, and by extension, when determining whether a point is within this <see cref="Triangle{TVector}"/></remarks>
    public readonly Single CADotBA;

    /// <summary>Determines whether a given point is within this <see cref="Triangle{TVector}"/></summary>
    /// <param name="point">The point to test against this <see cref="Triangle{TVector}"/>.</param>
    /// <returns><see langword="true"/> when <paramref name="point"/> is inside this <see cref="Triangle{TVector}"/>, else <see langword="false"/>.</returns>
    public Boolean Contains( TVector point )
    {
      var barycentric = ComputeBarycentric( point );
      return barycentric.X >= 0.0f && barycentric.Y >= 0.0f && barycentric.ComponentSum < 1.0f;
    }

    /// <summary>Calculates the barycentric coordinate of a point relative to this <see cref="Triangle{TVector}"/></summary>
    /// <param name="point">The point to calculate a barycentric coordinate for.</param>
    /// <returns>
    /// The barycentric coordinate of <paramref name="point"/> against this <see cref="Triangle{TVector}"/>, where:
    /// <list type="bullet">
    /// <item><see cref="A"/> is (0, 0).</item>
    /// <item><see cref="B"/> is (1, 0).</item>
    /// <item><see cref="C"/> is (0, 1).</item>
    /// </list>
    /// </returns>
    public Vector2 ComputeBarycentric( TVector point )
    {
      var pa = point.Subtract( A );

      var caDotPA = CA.Dot( pa );
      var baDotPa = BA.Dot( pa );

      var denominator = 1.0f / ( CAMagnitudeSquared * BAMagnitudeSquared - CADotBA * CADotBA );
      return new Vector2( CAMagnitudeSquared * baDotPa - CADotBA * caDotPA, BAMagnitudeSquared * caDotPA - CADotBA * baDotPa ) * denominator;
    }

    /// <inheritdoc />
    /// <param name="a">The value of <see cref="A"/>.</param>
    /// <param name="b">The value of <see cref="B"/>.</param>
    /// <param name="c">The value of <see cref="C"/>.</param>
    public Triangle( TVector a, TVector b, TVector c )
    {
      A = a;
      B = b;
      C = c;

      BA = b.Subtract( a );
      CA = c.Subtract( a );
      BAMagnitudeSquared = BA.MagnitudeSquared();
      CAMagnitudeSquared = CA.MagnitudeSquared();
      CADotBA = CA.Dot( BA );
    }
  }
}
