﻿// <copyright file = "Line.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 18:13:18 PM</date>
// <summary>A struct encapsulating the start and end point of a line.</summary>
using System;
using System.Runtime.Serialization;

namespace SUNRUSE.Vectors
{
	/// <summary>Encapsulates the <see cref="Start"/> and <see cref="End"/> of a line.</summary>
	/// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="Line{TVector}"/> is based.</typeparam>
  [DataContract( Name = "Line", Namespace = "" )]
	public struct Line<TVector> where TVector : struct, IVector<TVector>
	{
		/// <summary>The start of this <see cref="Line{TVector}"/>.</summary>
    [DataMember( Name = "Start", Order = 0, IsRequired = true )]
		public readonly TVector Start;

		/// <summary>The end of this <see cref="Line{TVector}"/>.</summary>
    [DataMember( Name = "End", Order = 1, IsRequired = true )]
		public readonly TVector End;

		/// <summary>The difference between the <see cref="Start"/> and the <see cref="End"/> of this <see cref="Line{TVector}"/>.</summary>
		public TVector Difference { get { return End.Subtract( Start ); } }

		/// <summary>The square of the distance between the <see cref="Start"/> and the <see cref="End"/> of this <see cref="Line{TVector}"/>.</summary>
		/// <remarks>Faster than <see cref="Length"/>.</remarks>
		public Single LengthSquared { get { return Difference.MagnitudeSquared(); } }

		/// <summary>The distance between the <see cref="Start"/> and the <see cref="End"/> of this <see cref="Line{TVector}"/>.</summary>
		/// <remarks>Slower than <see cref="Length"/>.</remarks>
		public Single Length { get { return Difference.Magnitude(); } }

		/// <summary>The normal between the <see cref="Start"/> and the <see cref="End"/> of this <see cref="Line{TVector}"/>.</summary>
		public TVector Normal { get { return Difference.Normalize(); } }

		/// <summary>Transforms this <see cref="Line{TVector}"/> by a given <see cref="ITransform{TVector}"/>.</summary>
		/// <typeparam name="TTransform">The <see cref="ITransform{TVector}"/> type to transform by.</typeparam>
		/// <param name="transform">The <typeparamref name="TTransform"/> to transform by.</param>
		/// <returns>A new <see cref="Line{TVector}"/> comprising <see langword="this"/> transformed by <paramref name="transform"/>.</returns>
		public Line<TVector> Transform<TTransform>( TTransform transform )
      where TTransform : struct, ITransform<TVector>
		{
			return new Line<TVector>( transform.Transform( Start ), transform.Transform( End ) );
		}

		/// <summary>Untransforms this <see cref="Line{TVector}"/> by a given <see cref="ITransform{TVector}"/>.</summary>
		/// <typeparam name="TTransform">The <see cref="ITransform{TVector}"/> type to untransform by.</typeparam>
		/// <param name="transform">The <typeparamref name="TTransform"/> to untransform by.</param>
		/// <returns>A new <see cref="Line{TVector}"/> comprising <see langword="this"/> untransformed by <paramref name="transform"/>.</returns>
		public Line<TVector> Untransform<TTransform>( TTransform transform )
      where TTransform : struct, ITransform<TVector>
		{
			return new Line<TVector>( transform.Untransform( Start ), transform.Untransform( End ) );
		}

		/// <inheritdoc />
		public override String ToString()
		{
			return String.Format( "Line<{0}>{{ Start: {1}, End: {2}, Length: {3} }}", 
				typeof( TVector ).Name, Start, End, Length );
		}

		/// <inheritdoc />
		/// <param name="start">The <see cref="Start"/> of the <see cref="Line{TVector}"/>.</param>
		/// <param name="end">The <see cref="End"/> of the <see cref="Line{TVector}"/>.</param>
		public Line( TVector start, TVector end )
		{
			Start = start;
			End = end;
		}
	}
}
