﻿// <copyright file = "VectorExtensions.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 15:48:21 AM</date>
// <summary>Extension methods for IVector implementations.</summary>
using System;

namespace SUNRUSE.Vectors
{
	/// <summary>Extension methods for <see cref="IVector{TVector}"/> implementations.</summary>
	public static class VectorExtensions
	{
		/// <summary>Calculates the dot product of two given <typeparamref name="TVector"/>s.</summary>
		/// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation a dot product is being calculated for.</typeparam>
		/// <param name="a">The first <typeparamref name="TVector"/> in the dot product.</param>
		/// <param name="b">The second <typeparamref name="TVector"/> in the dot product.</param>
		/// <returns>The dot product of <paramref name="a"/> and <paramref name="b"/>.</returns>
		public static Single Dot<TVector>( this TVector a, TVector b )
      where TVector : struct, IVector<TVector>
		{
			return a.Multiply( b ).ComponentSum;
		}

		/// <summary>Calculates the square of the magnitude of a given <typeparamref name="TVector"/>.</summary>
		/// <typeparam name="TVector">The <see cref="IVector{Vector}"/> implementation of which the square of the magnitude is being calculated.</typeparam>
		/// <param name="vector">A <typeparamref name="TVector"/> to calculate the square of the magnitude of.</param>
		/// <returns>The square of the magnitude of <paramref name="vector"/>.</returns>
		/// <remarks>Faster than <see cref="Magnitude{TVector}"/>.</remarks>
		public static Single MagnitudeSquared<TVector>( this TVector vector )
      where TVector : struct, IVector<TVector>
		{
			return vector.Dot( vector );
		}

		/// <summary>Calculates the magnitude of a given <typeparamref name="TVector"/>.</summary>
		/// <typeparam name="TVector">The <see cref="IVector{Vector}"/> implementation of which the magnitude is being calculated.</typeparam>
		/// <param name="vector">A <typeparamref name="TVector"/> to calculate the magnitude of.</param>
		/// <returns>The magnitude of <paramref name="vector"/>.</returns>
		/// <remarks>Slower than <see cref="MagnitudeSquared{TVector}"/>.</remarks>
		public static Single Magnitude<TVector>( this TVector vector )
      where TVector : struct, IVector<TVector>
		{
			return ( Single ) Math.Sqrt( vector.MagnitudeSquared() );
		}

		/// <summary>Normalizes a <see cref="IVector{TVector}"/>, creating a unit vector.</summary>
		/// <typeparam name="TVector">The type of <see cref="IVector{TVector}"/> to normalize.</typeparam>
		/// <param name="vector">The <typeparamref name="TVector"/> to normalize.</param>
		/// <returns><paramref name="vector"/>, normalized.</returns>
		public static TVector Normalize<TVector>( this TVector vector )
      where TVector : struct, IVector<TVector>
		{
			return vector.DivideBy( vector.Magnitude() );
		}

		/// <summary>Linearly interpolates between two <see cref="IVector{TVector}"/>s.</summary>
		/// <typeparam name="TVector">The type of <see cref="IVector{TVector}"/> to linearly interpolate.</typeparam>
		/// <param name="from">The <typeparamref name="TVector"/> to linearly interpolate from.</param>
		/// <param name="to">The <typeparamref name="TVector"/> to linearly interpolate to.</param>
		/// <param name="progress">The normalized interpolation progress, where 0.0f is <paramref name="from"/> and 1.0f is <paramref name="to"/>.</param>
		/// <returns>The linear interpolation from <paramref name="from"/> to <paramref name="to"/>, by <paramref name="progress"/>.</returns>
		public static TVector Interpolate<TVector>( this TVector from, TVector to, Single progress )
      where TVector : struct, IVector<TVector>
		{
			return from.Add( to.Subtract( from ).Multiply( progress ) );
		}

		/// <summary>Calculates the square of the distance between two <typeparamref name="TVector"/>s.
		/// </summary>
		/// <param name="from">The first <typeparamref name="TVector"/> to measure the square of the 
		/// distance between.</param>
		/// <param name="to">The second <typeparamref name="TVector"/> to measure the square of the 
		/// distance between.</param>
		/// <returns>The square of the distance between <paramref name="from"/> and 
		/// <paramref name="to"/>.</returns>
		/// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation being measured.
		/// </typeparam>
		/// <remarks>Faster than <see cref="Distance{TVector}"/>.</remarks>
		public static Single DistanceSquared<TVector>( this TVector from, TVector to )
			where TVector : struct, IVector<TVector>
		{
			return from.Subtract( to ).MagnitudeSquared();
		}

		/// <summary>Calculates the distance between two <typeparamref name="TVector"/>s.
		/// </summary>
		/// <param name="from">The first <typeparamref name="TVector"/> to measure the distance between.
		/// </param>
		/// <param name="to">The second <typeparamref name="TVector"/> to measure the distance 
		/// between.</param>
		/// <returns>The distance between <paramref name="from"/> and <paramref name="to"/>.</returns>
		/// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation being measured.
		/// </typeparam>
		/// <remarks>Slower than <see cref="DistanceSquared{TVector}"/>.</remarks>
		public static Single Distance<TVector>( this TVector from, TVector to )
			where TVector : struct, IVector<TVector>
		{
			return from.Subtract( to ).Magnitude();
		}

    /// <summary>Reflects a given <typeparamref name="TVector"/> against a given surface normal.</summary>
    /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation being reflected.
    /// </typeparam>
    /// <param name="reflect">The <typeparamref name="TVector"/> to reflect.</param>
    /// <param name="surfaceNormal">The surface normal <typeparamref name="TVector"/> to reflect against.</param>
    /// <returns><paramref name="reflect"/> reflected in <paramref name="surfaceNormal"/>.</returns>
    public static TVector Reflect<TVector>( this TVector reflect, TVector surfaceNormal)
      where TVector : struct, IVector<TVector>
    {
      return reflect.Subtract( surfaceNormal.Multiply( reflect.Dot( surfaceNormal ) * 2.0f ) );
    }

    /// <summary>Calculates the cross product of two given <see cref="Vector3"/>s.</summary>
    /// <param name="a">The first <see cref="Vector3"/> to cross product.</param>
    /// <param name="b">The second <see cref="Vector3"/> to cross product.</param>
    /// <returns>The cross product of <paramref name="a"/> and <paramref name="b"/>.</returns>
    public static Vector3 Cross(this Vector3 a, Vector3 b)
    {
      return new Vector3( a.Y * b.Z - a.Z * b.Y,
      a.Z * b.X - a.X * b.Z,
      a.X * b.Y - a.Y * b.X );
    }
	}
}
