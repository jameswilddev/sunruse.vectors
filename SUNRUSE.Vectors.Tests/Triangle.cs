﻿// <copyright file = "Triangle.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 17:38:22 PM</date>
// <summary>Tests for the Triangle struct.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;

namespace SUNRUSE.Vectors.Tests
{
  [TestFixture]
  public sealed class TriangleTests
  {
    [Test]
    public void Constructor()
    {
      var triangle = new Triangle<Vector2>( new Vector2( 3.0f, 4.0f ), new Vector2( 8.0f, 7.0f ), new Vector2( 4.0f, 1.0f ) );
      Assert.AreEqual( new Vector2( 3.0f, 4.0f ), triangle.A );
      Assert.AreEqual( new Vector2( 8.0f, 7.0f ), triangle.B );
      Assert.AreEqual( new Vector2( 4.0f, 1.0f ), triangle.C );
      Assert.AreEqual( new Vector2( 5.0f, 3.0f ), triangle.BA );
      Assert.AreEqual( 34.0f, triangle.BAMagnitudeSquared );
      Assert.AreEqual( new Vector2(1.0f, -3.0f), triangle.CA );
      Assert.AreEqual( 10.0f, triangle.CAMagnitudeSquared );
      Assert.AreEqual( -4.0f, triangle.CADotBA );
    }

    [Test]
    public void ComputeBarycentric()
    {
      var triangle = new Triangle<Vector2>( new Vector2( -6.0f, -8.0f ), new Vector2( 3.0f, 4.0f ), new Vector2( 9.0f, -5.0f ) );
      Assert.AreEqual( new Vector2( 0.0f, 0.0f ), triangle.ComputeBarycentric( new Vector2( -6.0f, -8.0f ) ) );
      Assert.AreEqual( new Vector2( 1.0f, 0.0f ), triangle.ComputeBarycentric( new Vector2( 3.0f, 4.0f ) ) );
      Assert.AreEqual( new Vector2( 0.0f, 1.0f ), triangle.ComputeBarycentric( new Vector2( 9.0f, -5.0f ) ) );
    }

    [Test]
    public void ContainsInside()
    {
      var triangle = new Triangle<Vector2>( new Vector2( 3.0f, 4.0f ), new Vector2( 8.0f, 7.0f ), new Vector2( 4.0f, 1.0f ) );
      Assert.IsTrue( triangle.Contains( new Vector2( 4.0f, 4.0f ) ) );
      Assert.IsTrue( triangle.Contains( new Vector2( 7.0f, 6.0f ) ) );
    }

    [Test]
    public void ContainsOutside()
    {
      var triangle = new Triangle<Vector2>( new Vector2( 3.0f, 4.0f ), new Vector2( 8.0f, 7.0f ), new Vector2( 4.0f, 1.0f ) );

      // Over AB.
      Assert.IsFalse( triangle.Contains( new Vector2( 5.0f, 6.0f ) ) );

      // Over BC.
      Assert.IsFalse( triangle.Contains( new Vector2( 7.0f, 5.0f ) ) );

      // Over CA.
      Assert.IsFalse( triangle.Contains( new Vector2( 3.0f, 3.0f ) ) );

      // Over AB and BC.
      Assert.IsFalse( triangle.Contains( new Vector2( 10.0f, 10.0f ) ) );

      // Over BC and CA.
      Assert.IsFalse( triangle.Contains( new Vector2( 4.0f, -2.0f ) ) );

      // Over CA and AB.
      Assert.IsFalse( triangle.Contains( new Vector2( 2.0f, 5.0f ) ) );
    }
  }
}
