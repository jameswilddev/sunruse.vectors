﻿using System;
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

namespace SUNRUSE.Vectors.Tests
{
	public static class VectorCheck
	{
    public static void AssertNearEqual( Vector2 a, Vector2 b )
    {
      var message = String.Format( "{0} and {1} were not within tolerance", a, b );

      Assert.AreEqual( a.X, b.X, 0.001f, message );
      Assert.AreEqual( a.Y, b.Y, 0.001f, message );
    }
		public static void AssertNearEqual( Vector3 a, Vector3 b )
		{
			var message = String.Format( "{0} and {1} were not within tolerance", a, b );

			Assert.AreEqual( a.X, b.X, 0.001f, message );
			Assert.AreEqual( a.Y, b.Y, 0.001f, message );
			Assert.AreEqual( a.Z, b.Z, 0.001f, message );
		}
	}
}

