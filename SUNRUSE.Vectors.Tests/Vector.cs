﻿// <copyright file = "Vector.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 11:49:20 AM</date>
// <summary>Tests for the generated Vector types.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace SUNRUSE.Vectors.Tests
{
	[TestFixture]
	public sealed class VectorTests
	{
		[Test]
		public void Constructor()
		{
			var vector = new Vector3( -2.0f, 887.4f, -9.2f );
			Assert.AreEqual( -2.0f, vector.X, 0.0001f );
			Assert.AreEqual( 887.4f, vector.Y, 0.0001f );
			Assert.AreEqual( -9.2f, vector.Z, 0.0001f );
		}

		[Test]
		public void ValueConstructor()
		{
			var vector = new Vector4( -5.0f );
			Assert.AreEqual( -5.0f, vector.X );
			Assert.AreEqual( -5.0f, vector.Y );
			Assert.AreEqual( -5.0f, vector.Z );
			Assert.AreEqual( -5.0f, vector.W );
		}
		//todo: test IVector
		[Test]
		public void Addition()
		{
			var vector = new Vector2( -8.0f, 1.0f ) + new Vector2( 8.0f, 10.0f );
			Assert.AreEqual( 0.0f, vector.X, 0.0001f );
			Assert.AreEqual( 11.0f, vector.Y, 0.0001f );
		}

		[Test]
		public void AdditionBySingle()
		{
			var vector = new Vector3( 9.0f, 3.0f, 2.0f ) + 3.0f;
			Assert.AreEqual( 12.0f, vector.X, 0.0001f );
			Assert.AreEqual( 6.0f, vector.Y, 0.0001f );
			Assert.AreEqual( 5.0f, vector.Z, 0.0001f );
		}

		[Test]
		public void AdditionToSingle()
		{
			var vector = 6.0f + new Vector3( -8.0f, 2.0f, -3.0f );
			Assert.AreEqual( -2.0f, vector.X, 0.0001f );
			Assert.AreEqual( 8.0f, vector.Y, 0.0001f );
			Assert.AreEqual( 3.0f, vector.Z, 0.0001f );
		}

		[Test]
		public void Subtraction()
		{
			var vector = new Vector2( 3.0f, -9.0f ) - new Vector2( -1.9f, -4.3f );
			Assert.AreEqual( 4.9f, vector.X, 0.0001f );
			Assert.AreEqual( -4.7f, vector.Y, 0.0001f );
		}

		[Test]
		public void SubtractionBySingle()
		{
			var vector = new Vector3( 8.4f, 2.9f, 1.4f ) - 4.0f;
			Assert.AreEqual( 4.4f, vector.X, 0.0001f );
			Assert.AreEqual( -1.1f, vector.Y, 0.0001f );
			Assert.AreEqual( -2.6f, vector.Z, 0.0001f );
		}

		[Test]
		public void SubtractionFromSingle()
		{
			var vector = 3.0f - new Vector3( 2.0f, -4.0f, 7.0f );
			Assert.AreEqual( 1.0f, vector.X, 0.0001f );
			Assert.AreEqual( 7.0f, vector.Y, 0.0001f );
			Assert.AreEqual( -4.0f, vector.Z, 0.0001f );
		}

		[Test]
		public void Multiplication()
		{
			var vector = new Vector2( 4.0f, -2.0f ) * new Vector2( 8.0f, -3.0f );
			Assert.AreEqual( 32.0f, vector.X );
			Assert.AreEqual( 6.0f, vector.Y );
		}

		[Test]
		public void Division()
		{
			var vector = new Vector2( 15.0f, 16.0f ) / new Vector2( 5.0f, 8.0f );
			Assert.AreEqual( 3.0f, vector.X );
			Assert.AreEqual( 2.0f, vector.Y );
		}

		[Test]
		public void ComponentSum()
		{
			Assert.AreEqual( 15.0f, new Vector3( -4.0f, 8.0f, 11.0f ).ComponentSum );
		}

		[Test]
		public new void ToString()
		{
			Assert.AreEqual( "Vector3{ X: -7.4, Y: 9.9, Z: 3.4 }", 
				new Vector3( -7.4f, 9.9f, 3.4f ).ToString() );
		}

    [Test]
    public void Truncate()
    {
      Assert.AreEqual( new Vector2( 4.0f, -8.0f ), new Vector3( 4.0f, -8.0f, 3.0f ).Truncate );
    }

    [Test]
    public void Append()
    {
      Assert.AreEqual( new Vector3( 9.0f, -8.0f, 4.0f ), new Vector2( 9.0f, -8.0f ).Append( 4.0f ) );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vector2 ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Vector2( 8.0f, -4.0f ) );

        Assert.AreEqual( @"<Vector2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<X>8</X>
	<Y>-4</Y>
</Vector2>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vector2 ) );
      using( var input = new StringReader( @"<Vector2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<X>8</X>
	<Y>-4</Y>
</Vector2>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Vector2 ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new Vector2( 8.0f, -4.0f ), deserialized );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'Y' from namespace '' is not expected. Expecting element 'X'." )]
    public void DeserializationComponentMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Vector2 ) );
      using( var input = new StringReader( @"<Vector2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Y>4</Y>
</Vector2>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }
	}
}

