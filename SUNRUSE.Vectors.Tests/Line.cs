﻿// <copyright file = "Line.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 18:26:34 PM</date>
// <summary>Tests for the Line struct.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;

namespace SUNRUSE.Vectors.Tests
{
  [TestFixture]
  public sealed class LineTests
  {
    [Test]
    public void Constructor()
    {
      var line = new Line<Vector3>( new Vector3( 4.0f, -8.0f, -10.0f ),
                   new Vector3( 32.0f, -18.0f, 9.0f ) );
      Assert.AreEqual( new Vector3( 4.0f, -8.0f, -10.0f ), line.Start );
      Assert.AreEqual( new Vector3( 32.0f, -18.0f, 9.0f ), line.End );
    }

    [Test]
    public void Difference()
    {
      Assert.AreEqual( new Vector3( -10.0f, 2.0f, 12.0f ),
        new Line<Vector3>( new Vector3( 8.0f, 6.0f, -3.0f ),
          new Vector3( -2.0f, 8.0f, 9.0f ) ).Difference );
    }

    [Test]
    public void LengthSquared()
    {
      Assert.AreEqual( 725.0f, new Line<Vector3>(
        new Vector3( -3.0f, -4.0f, 9.0f ),
        new Vector3( 20.0f, -18.0f, 9.0f ) ).LengthSquared, 0.0001f );
    }

    [Test]
    public void Length()
    {
      Assert.AreEqual( 18.1177f, new Line<Vector3>(
        new Vector3( -5.0f, 7.0f, 8.0f ),
        new Vector3( 8.5f, -4.0f, 3.0f ) ).Length, 0.0001f );
    }

    [Test]
    public void Normal()
    {
      VectorCheck.AssertNearEqual( new Vector3( -0.267261f, -0.908688f, 0.320713f ),
        new Line<Vector3>( new Vector3( 11.0f, 13.0f, -8.0f ),
          new Vector3( 6.0f, -4.0f, -2.0f ) ).Normal );
    }

    [Test]
    public void Transform()
    {
      var transform = new ITransformMock<Vector3>();

      var preTransform = new Line<Vector3>( new Vector3( 4.0f, -6.0f, -6.0f ),
                           new Vector3( 8.0f, -9.0f, -2.0f ) );

      transform.ExpectingTransform = true;
      transform.ExpectingVector = new Vector3( 4.0f, -6.0f, -6.0f );
      transform.Result = new Vector3( 9.0f, -2.0f, -3.0f );
      transform.AfterOperation = () =>
      {
        transform.ExpectingTransform = true;
        transform.ExpectingVector = new Vector3( 8.0f, -9.0f, -2.0f );
        transform.Result = new Vector3( 4.0f, 7.0f, -3.0f );
      };

      var postTransform = preTransform.Transform( transform.Transform );

      Assert.IsFalse( transform.ExpectingTransform );

      Assert.AreEqual( new Vector3( 9.0f, -2.0f, -3.0f ), postTransform.Start );
      Assert.AreEqual( new Vector3( 4.0f, 7.0f, -3.0f ), postTransform.End );
      Assert.AreEqual( new Vector3( -5.0f, 9.0f, 0.0f ), postTransform.Difference );
      Assert.AreEqual( 106.0f, postTransform.LengthSquared );
      Assert.AreEqual( 10.2956f, postTransform.Length, 0.0001f );
      VectorCheck.AssertNearEqual( new Vector3( -0.485643f, 0.874157f, 0.0f ),
        postTransform.Normal );
    }

    [Test]
    public void Untransform()
    {
      var transform = new ITransformMock<Vector3>();

      var preTransform = new Line<Vector3>( new Vector3( -4.0f, 5.0f, 7.0f ),
                           new Vector3( 7.0f, 4.0f, 6.0f ) );

      transform.ExpectingTransform = true;
      transform.ExpectingVector = new Vector3( -4.0f, 5.0f, 7.0f );
      transform.Result = new Vector3( 2.0f, -7.0f, -8.0f );
      transform.AfterOperation = () =>
      {
        transform.ExpectingTransform = true;
        transform.ExpectingVector = new Vector3( 7.0f, 4.0f, 6.0f );
        transform.Result = new Vector3( 9.0f, 11.0f, 13.0f );
      };

      var postTransform = preTransform.Transform( transform.Transform );

      Assert.IsFalse( transform.ExpectingTransform );

      Assert.AreEqual( new Vector3( 2.0f, -7.0f, -8.0f ), postTransform.Start );
      Assert.AreEqual( new Vector3( 9.0f, 11.0f, 13.0f ), postTransform.End );
      Assert.AreEqual( new Vector3( 7.0f, 18.0f, 21.0f ), postTransform.Difference );
      Assert.AreEqual( 814.0f, postTransform.LengthSquared );
      Assert.AreEqual( 28.5307f, postTransform.Length, 0.0001f );
      VectorCheck.AssertNearEqual( new Vector3( 0.24535f, 0.6309f, 0.73605f ),
        postTransform.Normal );
    }

    [Test]
    public new void ToString()
    {
      Assert.AreEqual( "Line<Vector2>{ Start: Vector2{ X: 4, Y: -2 }, "
      + "End: Vector2{ X: 7, Y: -3 }, Length: 3.162278 }",
        new Line<Vector2>( new Vector2( 4.0f, -2.0f ), new Vector2( 7.0f, -3.0f ) ).ToString() );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Line<Vector2> ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Line<Vector2>( new Vector2( 8.0f, -4.0f ), new Vector2( 2.0f, 4.0f ) ) );

        Assert.AreEqual( @"<Line xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Start>
		<X>8</X>
		<Y>-4</Y>
	</Start>
	<End>
		<X>2</X>
		<Y>4</Y>
	</End>
</Line>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Line<Vector2> ) );
      using( var input = new StringReader( @"<Line xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Start>
		<X>8</X>
		<Y>-4</Y>
	</Start>
	<End>
		<X>2</X>
		<Y>4</Y>
	</End>
</Line>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Line<Vector2> ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new Vector2( 8.0f, -4.0f ), deserialized.Start );
        Assert.AreEqual( new Vector2( 2.0f, 4.0f ), deserialized.End );

        Assert.AreEqual( new Vector2( -6.0f, 8.0f ), deserialized.Difference );
        Assert.AreEqual( 100.0f, deserialized.LengthSquared );
        Assert.AreEqual( 10.0f, deserialized.Length );
        VectorCheck.AssertNearEqual( new Vector2( -0.6f, 0.8f ), deserialized.Normal );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'End' from namespace '' is not expected. Expecting element 'Start'." )]
    public void DeserializationStartMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Line<Vector2> ) );
      using( var input = new StringReader( @"<Line xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<End>
		<X>2</X>
		<Y>4</Y>
	</End>
</Line>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 6 position 3. 'EndElement' 'Line' from namespace '' is not expected. Expecting element 'End'." )]
    public void DeserializationEndMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Line<Vector2> ) );
      using( var input = new StringReader( @"<Line xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Start>
		<X>2</X>
		<Y>4</Y>
	</Start>
</Line>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }
  }
}
