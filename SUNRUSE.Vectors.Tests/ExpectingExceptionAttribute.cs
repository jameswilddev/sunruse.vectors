﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if !NUNIT
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SUNRUSE.Vectors.Tests
{
  public sealed class ExpectingExceptionAttribute : ExpectedExceptionBaseAttribute
  {
    private readonly Type ExceptionType;
    public String ExpectedMessage { get; set; }

    protected override void Verify( Exception exception )
    {
      Assert.AreEqual( ExceptionType, exception.GetType() );
      Assert.AreEqual( ExpectedMessage, exception.Message );
    }

    public ExpectingExceptionAttribute(Type exceptionType)
    {
      ExceptionType = exceptionType;
    }
  }
}
#endif