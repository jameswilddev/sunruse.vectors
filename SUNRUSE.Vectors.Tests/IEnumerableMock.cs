﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace SUNRUSE.Vectors.Tests
{
	public sealed class IEnumerableMock<T> : IEnumerable<T>
	{
		public IEnumerable<T> Wrapped { private get; set; }

		public Boolean ExpectingEnumeration = false;

		public Action AfterEnumeration { private get; set; }

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			Assert.IsTrue( ExpectingEnumeration );
			ExpectingEnumeration = false;

			var result = Wrapped.GetEnumerator();

			var afterEnumeration = AfterEnumeration;
			AfterEnumeration = null;

			if( afterEnumeration != null )
				afterEnumeration();

			return result;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ( ( IEnumerable<T> ) this ).GetEnumerator();
		}
	}
}

