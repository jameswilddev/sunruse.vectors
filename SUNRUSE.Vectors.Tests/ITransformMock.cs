﻿using System;
using NUnit.Framework;

namespace SUNRUSE.Vectors.Tests
{
	public sealed class ITransformMock<TVector>
		where TVector : struct, IVector<TVector>
	{
		public Boolean ExpectingTransform = false;

		public Boolean ExpectingUntransform = false;

		public TVector ExpectingVector { private get; set; }

		public TVector Result { private get; set; }

		public Action AfterOperation { private get; set; }

		public struct TransformImplementation : ITransform<TVector>
		{
			private readonly ITransformMock<TVector> TransformMock;

			public TVector Transform( TVector transform )
			{
				Assert.IsTrue( TransformMock.ExpectingTransform );
				TransformMock.ExpectingTransform = false;

				Assert.AreEqual( TransformMock.ExpectingVector, transform );

				var afterOperation = TransformMock.AfterOperation;
				TransformMock.AfterOperation = null;

				var result = TransformMock.Result;

				if( afterOperation != null )
					afterOperation();

				return result;
			}

			public TVector Untransform( TVector transform )
			{
				Assert.IsTrue( TransformMock.ExpectingUntransform );
				TransformMock.ExpectingUntransform = false;

				Assert.AreEqual( TransformMock.ExpectingVector, transform );

				var afterOperation = TransformMock.AfterOperation;
				TransformMock.AfterOperation = null;

				var result = TransformMock.Result;

				if( afterOperation != null )
					afterOperation();

				return result;
			}

			internal TransformImplementation( ITransformMock<TVector> transformMock )
			{
				TransformMock = transformMock;
			}
		}

		public TransformImplementation Transform
		{
			get
			{
				return new TransformImplementation( this );
			}
		}
	}
}

