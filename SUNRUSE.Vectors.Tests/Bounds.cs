﻿// <copyright file = "Bounds.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 16:42:06 PM</date>
// <summary>Tests for the generated axis-aligned-bounding-boxes.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace SUNRUSE.Vectors.Tests
{
	[TestFixture]
	public sealed class BoundsTests
	{
		[Test]
		public void Constructor()
		{
			var bounds = new Bounds3( new Vector3( 5.0f, 6.0f, 2.0f ), 
				             new Vector3( -4.0f, 9.0f, 11.0f ) );
			Assert.AreEqual( new Vector3( 5.0f, 6.0f, 2.0f ), bounds.Minimum );
			Assert.AreEqual( new Vector3( -4.0f, 9.0f, 11.0f ), bounds.Maximum );
		}

		[Test]
		public void ContainsInside()
		{
			var bounds = new Bounds2( new Vector2( -2.0f, -1.0f ), 
				             new Vector2( 4.0f, 5.0f ) );
			Assert.IsTrue( bounds.Contains( new Vector2( -1.5f, -0.4f ) ) );
			Assert.IsTrue( bounds.Contains( new Vector2( 3.0f, -0.5f ) ) );
			Assert.IsTrue( bounds.Contains( new Vector2( -0.75f, 4.0f ) ) );
			Assert.IsTrue( bounds.Contains( new Vector2( 3.5f, 4.5f ) ) );
		}

		[Test]
		public void ContainsOnBorder()
		{
			var bounds = new Bounds2( new Vector2( -4.0f, -5.0f ), 
				             new Vector2( 3.0f, 2.0f ) );
			Assert.IsTrue( bounds.Contains( new Vector2( -4.0f, 0.5f ) ) );
			Assert.IsTrue( bounds.Contains( new Vector2( 3.0f, 0.5f ) ) );
		}

		[Test]
		public void ContainsOutside()
		{
			var bounds = new Bounds2( new Vector2( -5.0f, -2.0f ), 
				             new Vector2( 5.0f, 7.0f ) );
			Assert.IsFalse( bounds.Contains( new Vector2( 5.5f, 7.25f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( 5.0f, 7.5f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( 2.0f, 8.5f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -5.0f, 8.0f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -5.5f, 7.5f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -8.0f, 7.0f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -6.0f, 0.5f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -7.0f, -2.0f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -5.5f, -3.0f ) ) );
			Assert.IsFalse( bounds.Contains( new Vector2( -5.0f, -2.5f ) ) );
		}

    [Test]
    public void Size()
    {
      var bounds = new Bounds2( new Vector2( -5.0f, -2.0f ),
                     new Vector2( 5.0f, 7.0f ) );

      Assert.AreEqual( new Vector2( 10.0f, 9.0f ), bounds.Size );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Bounds2 ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new Bounds2( new Vector2( 8.0f, -4.0f ), new Vector2( 2.0f, 4.0f ) ) );

        Assert.AreEqual( @"<Bounds2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Minimum>
		<X>8</X>
		<Y>-4</Y>
	</Minimum>
	<Maximum>
		<X>2</X>
		<Y>4</Y>
	</Maximum>
</Bounds2>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Bounds2 ) );
      using( var input = new StringReader( @"<Bounds2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Minimum>
		<X>8</X>
		<Y>-4</Y>
	</Minimum>
	<Maximum>
		<X>2</X>
		<Y>4</Y>
	</Maximum>
</Bounds2>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( Bounds2 ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new Vector2( 8.0f, -4.0f ), deserialized.Minimum );
        Assert.AreEqual( new Vector2( 2.0f, 4.0f ), deserialized.Maximum );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'Maximum' from namespace '' is not expected. Expecting element 'Minimum'." )]
    public void DeserializationMinimumMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Bounds2 ) );
      using( var input = new StringReader( @"<Bounds2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Maximum>
		<X>2</X>
		<Y>4</Y>
	</Maximum>
</Bounds2>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 6 position 3. 'EndElement' 'Bounds2' from namespace '' is not expected. Expecting element 'Maximum'." )]
    public void DeserializationMaximumMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( Bounds2 ) );
      using( var input = new StringReader( @"<Bounds2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Minimum>
		<X>2</X>
		<Y>4</Y>
	</Minimum>
</Bounds2>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }
	}
}

