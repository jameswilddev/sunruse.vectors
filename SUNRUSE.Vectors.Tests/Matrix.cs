﻿// <copyright file = "Matrix.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 11:54:20 AM</date>
// <summary>Tests for the generated Matrix types.</summary>
#if NUNIT
using NUnit.Framework;

#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace SUNRUSE.Vectors.Tests
{
	[TestFixture]
	public sealed class MatrixTests
	{
		[Test]
		public void Constructor()
		{
			var matrix = new Matrix3( new Vector3( -4.0f, 8.0f, 2.0f ), 
				             new Vector3( 9.9f, -3.4f, -2.2f ), 
				             new Vector3( 5.0f, 7.0f, 4.0f ) );
			Assert.AreEqual( new Vector3( -4.0f, 8.0f, 2.0f ), matrix.X );
			Assert.AreEqual( new Vector3( 9.9f, -3.4f, -2.2f ), matrix.Y );
			Assert.AreEqual( new Vector3( 5.0f, 7.0f, 4.0f ), matrix.Z );
		}

		[Test]
		public void ExplicitConstructor()
		{
			var matrix = new Matrix2( 8.0f, -4.0f, 32.0f, 64.0f );
			Assert.AreEqual( new Vector2( 8.0f, -4.0f ), matrix.X );
			Assert.AreEqual( new Vector2( 32.0f, 64.0f ), matrix.Y );
		}

		[Test]
		public void Identity()
		{
			Assert.AreEqual( 
				new Matrix3( 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f ), 
				Matrix3.Identity );

			Assert.AreEqual( new Vector2( 4.0f, -8.0f ), 
				Matrix2.Identity.Transform( new Vector2( 4.0f, -8.0f ) ) );
			Assert.AreEqual( new Vector2( -3.0f, 7.0f ), 
				Matrix2.Identity.Untransform( new Vector2( -3.0f, 7.0f ) ) );
		}

		[Test]
		public void Transform()
		{
			Assert.AreEqual( new Vector2( 15.2f, -6.2f ), 
				new Matrix2( new Vector2( 4.0f, -3.0f ), new Vector2( -8.0f, -7.0f ) )
				.Transform( new Vector2( 3.0f, -0.4f ) ) );
		}

		[Test]
		public void Untransform()
		{
			Assert.AreEqual( new Vector2( 45.0f, 15.0f ), 
				new Matrix2( new Vector2( 7.0f, -4.0f ), new Vector2( 2.0f, 9.0f ) )
				.Untransform( new Vector2( 5.0f, 3.0f ) ) );
		}

		[Test]
		public void Serialization()
		{
			var dataContractSerializer = new DataContractSerializer( typeof( Matrix2 ) );
			using( var output = new StringWriter() )
				using( var writer = new XmlTextWriter( output ) {
					Formatting = Formatting.Indented,
					Indentation = 1,
					IndentChar = '\t'
				} )
				{
					dataContractSerializer.WriteObject( writer, new Matrix2( 3.0f, -8.0f, 7.0f, 6.0f ) );

					Assert.AreEqual( @"<Matrix2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<X>
		<X>3</X>
		<Y>-8</Y>
	</X>
	<Y>
		<X>7</X>
		<Y>6</Y>
	</Y>
</Matrix2>", output.GetStringBuilder().ToString() );
				}
		}

		[Test]
		public void Deserialization()
		{
			var dataContractSerializer = new DataContractSerializer( typeof( Matrix2 ) );
			using( var input = new StringReader( @"<Matrix2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<X>
    <X>3</X>
    <Y>-8</Y>
  </X>
  <Y>
    <X>7</X>
    <Y>6</Y>
  </Y>
</Matrix2>" ) )
				using( var reader = new XmlTextReader( input ) )
				{
					var deserialized = ( Matrix2 ) dataContractSerializer.ReadObject( reader );

					Assert.AreEqual( new Matrix2( 3.0f, -8.0f, 7.0f, 6.0f ), deserialized );
				}
		}

		[Test]
		[ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 2 position 3. 'Element' 'Y' from namespace '' is not expected. Expecting element 'X'." )]
		public void DeserializationComponentMissing()
		{
			var dataContractSerializer = new DataContractSerializer( typeof( Matrix2 ) );
			using( var input = new StringReader( @"<Matrix2 xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Y>4</Y>
</Matrix2>" ) )
				using( var reader = new XmlTextReader( input ) )
					dataContractSerializer.ReadObject( reader );
		}
	}
}