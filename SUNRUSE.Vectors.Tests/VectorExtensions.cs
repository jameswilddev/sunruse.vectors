﻿// <copyright file = "VectorExtensions.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>24/05/2014 15:55:18 AM</date>
// <summary>Tests for the IVector extension methods.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;

namespace SUNRUSE.Vectors.Tests
{
	[TestFixture]
	public sealed class VectorExtensionTests
	{
		[Test]
		public void Dot()
		{
			Assert.AreEqual( 27.0f, new Vector3( 4.0f, 7.0f, -9.0f ) 
				.Dot( new Vector3( 2.0f, -5.0f, -6.0f ) ) );
		}

		[Test]
		public void MagnitudeSquared()
		{
			Assert.AreEqual( 69.0f, new Vector3( -7.0f, 4.0f, 2.0f ).MagnitudeSquared() );
		}

		[Test]
		public void Magnitude()
		{
			Assert.AreEqual( 9.64365f, new Vector3( 8.0f, -5.0f, -2.0f ).Magnitude(), 0.0001f );
		}

		[Test]
		public void Normalize()
		{
			VectorCheck.AssertNearEqual( new Vector3( 0.839254f, -0.279751f, 0.466252f ),
				new Vector3( 9.0f, -3.0f, 5.0f ).Normalize() );
		}

		[Test]
		public void Interpolate()
		{
			VectorCheck.AssertNearEqual( new Vector3( 13.6f, -4.8f, -4.4f ), 
				new Vector3( 4.0f, -8.0f, 10.0f )
				.Interpolate( new Vector3( 16.0f, -4.0f, -8.0f ), 0.8f ) );
		}

		[Test]
		public void DistanceSquared()
		{
			Assert.AreEqual( 307.0f, new Vector3( 7.0f, -4.0f, 5.0f )
				.DistanceSquared( new Vector3( -8.0f, 5.0f, 4.0f ) ), 0.0001f );
		}

		[Test]
		public void Distance()
		{
			Assert.AreEqual( 7.6974f, new Vector3( 3.0f, -4.0f, 1.0f )
				.Distance( new Vector3( 9.5f, -3.0f, -3.0f ) ), 0.0001f );
		}

		[Test]
		public void Reflect()
		{
			// Reflect head on.
			VectorCheck.AssertNearEqual( new Vector3( 4.0f, -8.0f, -7.0f ),
				new Vector3( -4.0f, 8.0f, 7.0f ).Reflect( new Vector3( 0.35218f, -0.704361f, -0.616316f ) ) );

			// Reflect surface facing along axis.
			VectorCheck.AssertNearEqual( new Vector3( -9.0f, 8.0f, -3.0f ),
				new Vector3( -9.0f, 8.0f, 3.0f ).Reflect( new Vector3( 0.0f, 0.0f, 1.0f ) ) );

			// Reflect in 45-degree plane.
			VectorCheck.AssertNearEqual( new Vector3( 4.0f, 0.0f, -8.0f ),
				new Vector3( 0.0f, 4.0f, -8.0f ).Reflect( new Vector3( 0.707107f, -0.707107f, 0.0f ) ) );
		}

    [Test]
    public void CrossProduct()
    {
      VectorCheck.AssertNearEqual( new Vector3( -7.0f, 7.0f, -2.0f ), new Vector3( 3.0f, 5.0f, 7.0f ).Cross( new Vector3( 4.0f, 6.0f, 7.0f ) ) );
    }
	}
}

