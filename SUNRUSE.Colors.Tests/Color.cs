﻿// <copyright file = "Color.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/08/2014 21:13:42 PM</date>
// <summary>Tests for the generated Color types.</summary>
#if NUNIT
using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif
using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace SUNRUSE.Colors.Tests
{
  using Vectors.Tests;

  [TestFixture]
  public sealed class ColorTests
  {
    [Test]
    public void Constructor()
    {
      var color = new ColorRGB( 87, 19, 209 );
      Assert.AreEqual( 87, color.Red );
      Assert.AreEqual( 19, color.Green );
      Assert.AreEqual( 209, color.Blue );
    }

    [Test]
    public void Serialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( ColorRGB ) );
      using( var output = new StringWriter() )
      using( var writer = new XmlTextWriter( output )
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      } )
      {
        dataContractSerializer.WriteObject( writer, new ColorRGB( 98, 26, 197 ) );

        Assert.AreEqual( @"<ColorRGB xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Red>98</Red>
	<Green>26</Green>
	<Blue>197</Blue>
</ColorRGB>", output.GetStringBuilder().ToString() );
      }
    }

    [Test]
    public void Deserialization()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( ColorRGB ) );
      using( var input = new StringReader( @"<ColorRGB xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Red>98</Red>
	<Green>26</Green>
	<Blue>197</Blue>
</ColorRGB>" ) )
      using( var reader = new XmlTextReader( input ) )
      {
        var deserialized = ( ColorRGB ) dataContractSerializer.ReadObject( reader );

        Assert.AreEqual( new ColorRGB( 98, 26, 197 ), deserialized );
      }
    }

    [Test]
    [ExpectedException( typeof( SerializationException ), ExpectedMessage = "Error in line 3 position 3. 'Element' 'Blue' from namespace '' is not expected. Expecting element 'Green'." )]
    public void DeserializationComponentMissing()
    {
      var dataContractSerializer = new DataContractSerializer( typeof( ColorRGB ) );
      using( var input = new StringReader( @"<ColorRGB xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
	<Red>98</Red>
	<Blue>197</Blue>
</ColorRGB>" ) )
      using( var reader = new XmlTextReader( input ) )
        dataContractSerializer.ReadObject( reader );
    }

    [Test]
    public void ToString()
    {
      Assert.AreEqual( "ColorRGB{ Red: 98, Green: 26, Blue: 197 }", new ColorRGB( 98, 26, 197 ).ToString() );
    }
  }
}