﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Scenes
{
  using Vectors;

  //todo: implement and test

  /// <summary>An implementation of <see cref="IFalloff{TVector}"/> which is a spherical or circular falloff from a point.</summary>
  /// <remarks>This is faster than <see cref="FalloffRadial{TVector}"/>.</remarks>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="FalloffRadialNonLinear{TVector}"/> is based.</typeparam>
  public sealed class FalloffRadialNonLinear<TVector> : IFalloff<TVector> where TVector : struct, IVector<TVector>
  {
    /// <summary>The origin of this <see cref="FalloffRadialNonLinear{TVector}"/>.</summary>
    public readonly TVector Origin;

    /// <summary>The radius of this <see cref="FalloffRadialNonLinear{TVector}"/>.</summary>
    public readonly Single Radius;

    /// <summary>The magnitude of this <see cref="FalloffRadialNonLinear{TVector}"/>.</summary>
    public readonly Single Magnitude;

    TVector IFalloff<TVector>.Sample( TVector at )
    {
      throw new NotImplementedException();
    }

    void IFalloff<TVector>.Sample( TVector at, out TVector normal, out Single magnitude )
    {
      throw new NotImplementedException();
    }

    Single IFalloff<TVector>.SampleMagnitude( TVector a )
    {
      throw new NotImplementedException();
    }

    /// <inheritdoc />
    /// <param name="vector">The value of <see cref="Origin"/>.</param>
    /// <param name="radius">The value of <see cref="Radius"/>.</param>
    /// <param name="magnitude">The value of <see cref="Magnitude"/>.</param>
    public FalloffRadialNonLinear( TVector origin, Single radius, Single magnitude )
    {
      throw new NotImplementedException();
    }
  }
}
