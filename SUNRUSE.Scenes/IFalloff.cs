﻿// <copyright file = "IFalloff.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 07:33:28 PM</date>
// <summary>An interface for classes which describe a falloff.</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Scenes
{
  using Vectors;

  /// <summary>An interface to be implemented by falloff classes, describing the strength and direction of an effect at given points in space.</summary>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="Line{TVector}"/> is based.</typeparam>
  public interface IFalloff<TVector> where TVector : struct, IVector<TVector>
  {
    /// <summary>Retrieves the vector (normal * magnitude) of this <see cref="IFalloff{TVector}"/> at a given position.</summary>
    /// <param name="at">The location to sample at.</param>
    /// <returns>The vector (normal * magnitude) at <paramref name="at"/>.</returns>
    TVector Sample( TVector at );

    /// <summary>Retrieves the normal and magnitude of this <see cref="IFalloff{TVector}"/> at a given position.</summary>
    /// <param name="at">The location to sample at.</param>
    /// <param name="normal">The normal at <paramref name="at"/>.</param>
    /// <param name="magnitude">The magnitude at <paramref name="at"/>.</param>
    void Sample( TVector at, out TVector normal, out Single magnitude );

    /// <summary>Retrieves the vector magnitude of this <see cref="IFalloff{TVector}"/> at a given position.</summary>
    /// <param name="at">The location to sample at.</param>
    /// <returns>The vector magnitude at <paramref name="at"/>.</returns>
    Single SampleMagnitude( TVector a );
  }
}
