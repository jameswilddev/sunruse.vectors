﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Scenes
{
  using Vectors;

  /// <summary>An implementation of <see cref="IFalloff{TVector}"/> which is always a specific magnitude and normal regardless of where sampled.</summary>
  /// <typeparam name="TVector">The <see cref="IVector{TVector}"/> implementation on which this <see cref="FalloffAmbient{TVector}"/> is based.</typeparam>
  public sealed class FalloffAmbient<TVector> : IFalloff<TVector> where TVector : struct, IVector<TVector>
  {
    /// <summary>The vector of this <see cref="FalloffAmbient{TVector}"/>.  (normal * magnitude)</summary>
    public readonly TVector Vector;

    private readonly Single Magnitude;
    private readonly TVector Normal;

    TVector IFalloff<TVector>.Sample( TVector at )
    {
      return Vector;
    }

    void IFalloff<TVector>.Sample( TVector at, out TVector normal, out Single magnitude )
    {
      normal = Normal;
      magnitude = Magnitude;
    }

    Single IFalloff<TVector>.SampleMagnitude( TVector a )
    {
      return Magnitude;
    }

    /// <inheritdoc />
    /// <param name="vector">The value of <see cref="Vector"/>.</param>
    public FalloffAmbient(TVector vector)
    {
      Vector = vector;
      Normal = vector.Normalize();
      Magnitude = vector.Magnitude();
    }
  }
}
