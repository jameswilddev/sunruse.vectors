﻿// <copyright file = "Cache.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>15/06/2014 09:40:11 PM</date>
// <summary>A class for caching objects of arbitrary types by their type.</summary>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Scenes
{
  using Vectors;

  /// <summary>A collection of <see cref="Object"/>s, sorted by <see cref="Type"/>, representing a set of entities cached in a section of a <see cref="Scene{TVector}"/>.</summary>
  /// <remarks>This allows for type-safe storage of arbitrary types in a <see cref="Scene{TVector}"/> which can then by retrieved by type.</remarks>
  public sealed class Cache
  {
    private readonly ReadOnlyDictionary<Type, IEnumerable> Caches;

    /// <summary>Retrieves the cached items of a given <see cref="Type"/>.</summary>
    /// <typeparam name="T">The <see cref="Type"/> of cached item to retrieve.</typeparam>
    /// <returns>A set of cached items of type <typeparamref name="T"/>.</returns>
    public IEnumerable<T> GetEntities<T>()
    {
      IEnumerable output;
      if( Caches.TryGetValue( typeof( T ), out output ) )
        return ( IEnumerable<T> ) output;
      else
        return Enumerable.Empty<T>();
    }

    /// <inheritdoc />
    /// <param name="entities">The items to cache.</param>
    /// <exception cref="ArgumentNullException">Thrown when <paramref name="entities"/> or its contents are <see langword="null"/>.</exception>
    public Cache( IEnumerable<Object> entities )
    {
      if( entities == null )
        throw new ArgumentNullException( "entities" );

      var types = new Dictionary<Type, List<Object>>();

      foreach( var entity in entities )
      {
        if( entity == null )
          throw new ArgumentNullException( "entities", "Cannot contain null" );

        if( !types.ContainsKey( entity.GetType() ) )
          types[ entity.GetType() ] = new List<Object>();

        types[ entity.GetType() ].Add( entity );
      }

      var castMethod = typeof( Enumerable ).GetMethod( "Cast" );
      var toListMethod = typeof( Enumerable ).GetMethod( "ToList" );

      var cache = new Dictionary<Type, IEnumerable>();
      foreach( var type in types )
      {
        var cast = castMethod.MakeGenericMethod( new[] { type.Key } ).Invoke( null, new[] { type.Value } );
        var toList = toListMethod.MakeGenericMethod( new[] { type.Key } ).Invoke( null, new[] { cast } );
        var asReadOnlyMethod = toList.GetType().GetMethod( "AsReadOnly" );
        var asReadOnly = asReadOnlyMethod.Invoke( toList, new Object[0] );
        cache[ type.Key ] = (IEnumerable)asReadOnly;
      }
      Caches = new ReadOnlyDictionary<Type, IEnumerable>( cache );
    }
  }
}
