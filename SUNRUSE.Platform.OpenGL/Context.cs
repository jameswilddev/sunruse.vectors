﻿// <copyright file = "Context.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 09:19:11 PM</date>
// <summary>An IContext implementation based on OpenGL.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace SUNRUSE.Platform.OpenGL
{
  using Vectors;

  /// <summary>Implements <see cref="IContext"/> using an <see cref="IInterop"/> implementation.</summary>
  [Export( typeof( IContext ) )]
  public sealed class Context : IContext
  {
    private readonly IInterop Interop;

    void IContext.Clear( Vector3? color, Boolean depth )
    {
      if( !color.HasValue && !depth )
        return;

      if( color.HasValue )
        Interop.ClearColor( color.Value.X, color.Value.Y, color.Value.Z, 0.0f );

      Interop.Enable( Constants.SCISSOR_TEST );
      Interop.Clear( ( color.HasValue ? Constants.COLOR_BUFFER_BIT : 0 ) | ( depth ? Constants.DEPTH_BUFFER_BIT : 0 ) );
      Interop.Disable( Constants.SCISSOR_TEST );
    }

    void IContext.Viewport( Bounds2 viewport )
    {
      Interop.Viewport( ( int ) viewport.Minimum.X, ( int ) viewport.Minimum.Y, ( int ) viewport.Size.X, ( int ) viewport.Size.Y );
      Interop.Scissor( ( int ) viewport.Minimum.X, ( int ) viewport.Minimum.Y, ( int ) viewport.Size.X, ( int ) viewport.Size.Y );
    }

    /// <inheritdoc />
    /// <param name="interop">The implementation of <see cref="IOpenGLInterop"/> to use.</param>
    [ImportingConstructor]
    public Context( IInterop interop )
    {
      if( interop == null )
        throw new ArgumentNullException( "interop" );

      Interop = interop;
    }
  }
}
