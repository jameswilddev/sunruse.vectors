﻿// <copyright file = "IInterop.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 09:19:35 PM</date>
// <summary>An interface used for mocking OpenGL native interop.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUNRUSE.Platform.OpenGL
{
  /// <summary>An interface for wrapping the OpenGL API for testing purposes.</summary>
  public interface IInterop
  {
    /// <summary>Sets the area of the screen to render to.  Normalized device coordinates are adjusted to fit this area.</summary>
    /// <param name="x">The distance from the left border of the screen to the left border of the viewport, in pixels.</param>
    /// <param name="y">The distance from the bottom border of the screen to the bottom border of the viewport, in pixels.</param>
    /// <param name="width">The width of the viewport in pixels.</param>
    /// <param name="height">The height of the viewport in pixels.</param>
    void Viewport( int x, int y, int width, int height );

    /// <summary>Sets the area of the screen to render to.  The image is cropped to this area.</summary>
    /// <param name="x">The distance from the left border of the screen to the left border of the scissor area, in pixels.</param>
    /// <param name="y">The distance from the bottom border of the screen to the bottom border of the scissor area, in pixels.</param>
    /// <param name="width">The width of the scissor area in pixels.</param>
    /// <param name="height">The height of the scissor area in pixels.</param>
    void Scissor( int x, int y, int width, int height );

    /// <summary>Clears the render buffer.</summary>
    /// <remarks>Not limited to the area specified by <see cref="Viewport"/>.</remarks>
    /// <param name="mask">The buffers (<see cref="OpenGLConstants"/>.GL_*_BUFFER_BIT) to clear.</param>
    void Clear( int mask );

    /// <summary>Sets the color to <see cref="Clear"/> with.</summary>
    /// <param name="red">The intensity of the red channel, where 1.0f is full.</param>
    /// <param name="green">The intensity of the green channel, where 1.0f is full.</param>
    /// <param name="blue">The intensity of the blue channel, where 1.0f is full.</param>
    /// <param name="alpha">The intensity of the alpha channel, where 1.0f is full.</param>
    void ClearColor( Single red, Single green, Single blue, Single alpha );

    /// <summary>Enables a feature.</summary>
    /// <param name="cap">The feature to enable.</param>
    void Enable( int cap );

    /// <summary>Disables a feature.</summary>
    /// <param name="cap">The feature to disable.</param>
    void Disable( int cap );
  }
}
