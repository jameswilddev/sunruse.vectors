﻿// <copyright file = "Interop.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 09:19:49 PM</date>
// <summary>The runtime implementation of IInterop.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Runtime.InteropServices;

namespace SUNRUSE.Platform.OpenGL
{
  /// <summary>An implementation of <see cref="IInterop"/> which contacts the OpenGL API.</summary>
  [Export( typeof( IInterop ) )]
  public sealed class Interop : IInterop
  {
    /// <inheritdoc />
    void IInterop.Viewport( int x, int y, int width, int height )
    {
      glViewport( x, y, width, height );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glViewport( int x, int y, int width, int height );

    /// <inheritdoc />
    void IInterop.Scissor( int x, int y, int width, int height )
    {
      glScissor( x, y, width, height );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glScissor( int x, int y, int width, int height );

    /// <inheritdoc />
    void IInterop.Clear( int mask )
    {
      glClear( mask );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glClear( int mask );

    /// <inheritdoc />
    void IInterop.ClearColor( Single red, Single green, Single blue, Single alpha )
    {
      glClearColor( red, green, blue, alpha );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glClearColor( Single red, Single green, Single blue, Single alpha );

    /// <inheritdoc />
    void IInterop.Enable( int cap )
    {
      glEnable( cap );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glEnable( int cap );

    /// <inheritdoc />
    void IInterop.Disable( int cap )
    {
      glDisable( cap );
    }

    [DllImport( "OpenGL32.dll" )]
    private static extern void glDisable( int cap );
  }
}
