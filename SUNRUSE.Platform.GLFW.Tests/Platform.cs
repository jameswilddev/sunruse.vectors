﻿// <copyright file = "Platform.cs" company="SUNRUSE">
// Copyright (c) 2014 SUNRUSE 
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>James Wild</author>
// <date>21/06/2014 18:16:28 AM</date>
// <summary>Tests for the GLFW implementation of IPlatform.</summary>
#if NUNIT
  using NUnit.Framework;
#else
using TestFixture = Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute;
using Test = Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using ExpectedException = SUNRUSE.Vectors.Tests.ExpectingExceptionAttribute;
#endif

using System;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

namespace SUNRUSE.Platform.GLFW.Tests
{
  using Vectors;
  using Vectors.Tests;

  [TestFixture]
  public sealed class PlatformTests
  {
    public sealed class InteropMock : IInterop
    {
      public Func<Boolean> ValidateInit = null;

      Boolean IInterop.Init()
      {
        return ValidateInit();
      }

      public Action ValidateTerminate = null;

      void IInterop.Terminate()
      {
        ValidateTerminate();
      }

      public Func<int, int, int, int, int, int, int, int, int, Boolean> ValidateOpenWindow = null;

      Boolean IInterop.OpenWindow( int width, int height, int redbits, int greenbits, int bluebits, int alphabits, int depthbits, int stencilbits, int mode )
      {
        return ValidateOpenWindow( width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, mode );
      }

      public Action<String> ValidateSetWindowTitle = null;

      void IInterop.SetWindowTitle( String title )
      {
        ValidateSetWindowTitle( title );
      }

      public Action ValidateSwapBuffers = null;

      void IInterop.SwapBuffers()
      {
        ValidateSwapBuffers();
      }

      public Func<int, Boolean> ValidateGetWindowParam = null;

      Boolean IInterop.GetWindowParam( int param )
      {
        return ValidateGetWindowParam( param );
      }
    }

    public sealed class SettingsMock : ISettings
    {
      public Func<int> ValidateScreenWidthGet = null;
      public Action<int> ValidateScreenWidthSet = null;

      int ISettings.ScreenWidth
      {
        get { return ValidateScreenWidthGet(); }
        set { ValidateScreenWidthSet( value ); }
      }

      public Func<int> ValidateScreenHeightGet = null;
      public Action<int> ValidateScreenHeightSet = null;

      int ISettings.ScreenHeight
      {
        get { return ValidateScreenHeightGet(); }
        set { ValidateScreenHeightSet( value ); }
      }

      public Func<Boolean> ValidateFullscreenGet = null;
      public Action<Boolean> ValidateFullscreenSet = null;

      Boolean ISettings.Fullscreen
      {
        get { return ValidateFullscreenGet(); }
        set { ValidateFullscreenSet( value ); }
      }

      public Action ValidateSave = null;

      void ISettings.Save()
      {
        ValidateSave();
      }
    }

    public sealed class SimulationMock : ISimulation
    {
      public Func<String> ValidateWindowTitleGet = null;

      String ISimulation.WindowTitle { get { return ValidateWindowTitleGet(); } }

      public Action ValidateStart = null;

      void ISimulation.Start()
      {
        ValidateStart();
      }

      public Action<int, int> ValidateRender = null;

      void ISimulation.Render( int screenWidth, int screenHeight )
      {
        ValidateRender( screenWidth, screenHeight );
      }
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage = @"Value cannot be null.
Parameter name: interop" )]
    public void ConstructorInteropNullThrowsException()
    {
      new Platform( null, null, null );
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage = @"Value cannot be null.
Parameter name: settings" )]
    public void ConstructorSettingsNullThrowsException()
    {
      new Platform( new InteropMock(), null, null );
    }

    [Test]
    [ExpectedException( typeof( ArgumentNullException ), ExpectedMessage = @"Value cannot be null.
Parameter name: simulation" )]
    public void ConstructorSimultationNullThrowsException()
    {
      new Platform( new InteropMock(), new SettingsMock(), null );
    }

    [Test]
    public void ConstructorWithoutRunDoesNothing()
    {
      new Platform( new InteropMock(), new SettingsMock(), new SimulationMock() );
    }

    [Test]
    public void RunWindowed()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var init = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;
      var openWindow = false;
      var windowTitleGet = false;
      var setWindowTitle = false;
      var getWindowOpen = 0;
      var render = 0;
      var swapBuffers = 0;
      var terminate = false;

      interop.ValidateInit = () =>
      {
        Assert.IsFalse( init );
        init = true;
        return true;
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      interop.ValidateOpenWindow = ( width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, mode ) =>
      {
        Assert.AreEqual( 640, width );
        Assert.AreEqual( 480, height );
        Assert.AreEqual( 8, redbits );
        Assert.AreEqual( 8, greenbits );
        Assert.AreEqual( 8, bluebits );
        Assert.AreEqual( 0, alphabits );
        Assert.AreEqual( 24, depthbits );
        Assert.AreEqual( 0, stencilbits );
        Assert.AreEqual( Constants.WINDOW, mode );

        Assert.IsTrue( fullscreenGet );
        Assert.IsTrue( screenWidthGet );
        Assert.IsTrue( screenHeightGet );

        Assert.IsFalse( openWindow );
        openWindow = true;

        return true;
      };

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      interop.ValidateSetWindowTitle = ( String title ) =>
      {
        Assert.AreEqual( "Test Window Title", title );

        Assert.IsTrue( windowTitleGet );
        Assert.IsFalse( setWindowTitle );
        setWindowTitle = true;
      };

      interop.ValidateGetWindowParam = ( int param ) =>
      {
        Assert.AreEqual( Constants.OPENED, param );
        Assert.IsTrue( openWindow );

        Assert.IsFalse( getWindowOpen > 3 );
        Assert.IsFalse( getWindowOpen > render );
        Assert.IsFalse( getWindowOpen > swapBuffers );

        getWindowOpen++;

        return getWindowOpen < 3;
      };

      simulation.ValidateRender = ( int width, int height ) =>
      {
        Assert.AreEqual( 640, width );
        Assert.AreEqual( 480, height );

        Assert.IsTrue( openWindow );

        Assert.IsFalse( render > 2 );
        Assert.IsTrue( ( render + 1 ) == getWindowOpen );
        Assert.IsFalse( render > swapBuffers );

        render++;
      };

      interop.ValidateSwapBuffers = () =>
      {
        Assert.IsTrue( openWindow );

        Assert.IsFalse( swapBuffers > 2 );
        Assert.IsTrue( ( swapBuffers + 1 ) == getWindowOpen );
        Assert.IsTrue( ( swapBuffers + 1 ) == render );

        swapBuffers++;
      };

      interop.ValidateTerminate = () =>
      {
        Assert.AreEqual( 3, getWindowOpen );
        Assert.AreEqual( 2, render );
        Assert.AreEqual( 2, swapBuffers );

        Assert.IsFalse( terminate );
        terminate = true;
      };

      platform.Run();

      Assert.IsTrue( terminate );
    }

    [Test]
    public void RunFullscreen()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var init = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;
      var openWindow = false;
      var windowTitleGet = false;
      var setWindowTitle = false;
      var getWindowOpen = 0;
      var render = 0;
      var swapBuffers = 0;
      var terminate = false;

      interop.ValidateInit = () =>
      {
        Assert.IsFalse( init );
        init = true;
        return true;
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return true;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 512;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 384;
      };

      interop.ValidateOpenWindow = ( width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, mode ) =>
      {
        Assert.AreEqual( 512, width );
        Assert.AreEqual( 384, height );
        Assert.AreEqual( 8, redbits );
        Assert.AreEqual( 8, greenbits );
        Assert.AreEqual( 8, bluebits );
        Assert.AreEqual( 0, alphabits );
        Assert.AreEqual( 24, depthbits );
        Assert.AreEqual( 0, stencilbits );
        Assert.AreEqual( Constants.FULLSCREEN, mode );

        Assert.IsTrue( fullscreenGet );
        Assert.IsTrue( screenWidthGet );
        Assert.IsTrue( screenHeightGet );

        Assert.IsFalse( openWindow );
        openWindow = true;

        return true;
      };

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      interop.ValidateSetWindowTitle = ( String title ) =>
      {
        Assert.AreEqual( "Test Window Title", title );

        Assert.IsTrue( windowTitleGet );
        Assert.IsFalse( setWindowTitle );
        setWindowTitle = true;
      };

      interop.ValidateGetWindowParam = ( int param ) =>
      {
        Assert.AreEqual( Constants.OPENED, param );
        Assert.IsTrue( openWindow );

        Assert.IsFalse( getWindowOpen > 3 );
        Assert.IsFalse( getWindowOpen > render );
        Assert.IsFalse( getWindowOpen > swapBuffers );

        getWindowOpen++;

        return getWindowOpen < 3;
      };

      simulation.ValidateRender = ( int width, int height ) =>
      {
        Assert.AreEqual( 512, width );
        Assert.AreEqual( 384, height );

        Assert.IsTrue( openWindow );

        Assert.IsFalse( render > 2 );
        Assert.IsTrue( ( render + 1 ) == getWindowOpen );
        Assert.IsFalse( render > swapBuffers );

        render++;
      };

      interop.ValidateSwapBuffers = () =>
      {
        Assert.IsTrue( openWindow );

        Assert.IsFalse( swapBuffers > 2 );
        Assert.IsTrue( ( swapBuffers + 1 ) == getWindowOpen );
        Assert.IsTrue( ( swapBuffers + 1 ) == render );

        swapBuffers++;
      };

      interop.ValidateTerminate = () =>
      {
        Assert.AreEqual( 3, getWindowOpen );
        Assert.AreEqual( 2, render );
        Assert.AreEqual( 2, swapBuffers );

        Assert.IsFalse( terminate );
        terminate = true;
      };

      platform.Run();

      Assert.IsTrue( terminate );
    }

    [Test]
    [ExpectedException( typeof( ExternalException ), ExpectedMessage = "GLFW failed to initialize" )]
    public void RunInitFail()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      var init = false;
      var terminate = false;

      interop.ValidateInit = () =>
      {
        Assert.IsTrue( windowTitleGet );
        Assert.IsTrue( screenWidthGet );
        Assert.IsTrue( screenHeightGet );

        Assert.IsFalse( init );
        init = true;
        return false;
      };

      interop.ValidateTerminate = () =>
      {
        Assert.IsTrue( init );
        Assert.IsFalse( terminate );
        terminate = true;
      };

      try
      {
        platform.Run();
      }
      finally
      {
        Assert.IsTrue( terminate );
      }
    }

    [Test]
    [ExpectedException( typeof( ExternalException ), ExpectedMessage = "GLFW failed to open a context" )]
    public void RunOpenWindowFail()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      var init = false;
      var openWindow = false;
      var terminate = false;

      interop.ValidateInit = () =>
      {
        Assert.IsFalse( init );
        init = true;
        return true;
      };

      interop.ValidateOpenWindow = ( width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, mode ) =>
      {
        Assert.IsTrue( screenWidthGet );
        Assert.IsTrue( screenHeightGet );
        Assert.IsTrue( fullscreenGet );
        Assert.IsTrue( windowTitleGet );
        Assert.IsTrue( init );

        Assert.IsFalse( openWindow );
        openWindow = true;

        return false;
      };

      interop.ValidateTerminate = () =>
      {
        Assert.IsTrue( openWindow );
        Assert.IsFalse( terminate );
        terminate = true;
      };

      try
      {
        platform.Run();
      }
      finally
      {
        Assert.IsTrue( terminate );
      }
    }

    public sealed class SuccessException : Exception
    {
      public SuccessException()
        : base( "" )
      {
      }
    }

    [Test]
    [ExpectedException( typeof( SuccessException ), ExpectedMessage = "" )]
    public void RunRenderThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      var init = false;
      var openWindow = false;
      var setWindowTitle = false;
      var getWindowParam = false;
      var render = false;
      var terminate = false;

      interop.ValidateInit = () =>
      {
        Assert.IsFalse( init );
        init = true;
        return true;
      };

      interop.ValidateOpenWindow = ( width, height, redbits, greenbits, bluebits, alphabits, depthbits, stencilbits, mode ) =>
      {
        Assert.IsTrue( screenWidthGet );
        Assert.IsTrue( screenHeightGet );
        Assert.IsTrue( fullscreenGet );
        Assert.IsTrue( windowTitleGet );
        Assert.IsTrue( init );

        Assert.IsFalse( openWindow );
        openWindow = true;

        return true;
      };

      interop.ValidateSetWindowTitle = ( String title ) =>
      {
        Assert.IsTrue( openWindow );
        Assert.IsFalse( setWindowTitle );
        setWindowTitle = true;
      };

      interop.ValidateGetWindowParam = ( int param ) =>
      {
        Assert.AreEqual( Constants.OPENED, param );
        Assert.IsTrue( setWindowTitle );
        Assert.IsFalse( getWindowParam );
        getWindowParam = true;
        return true;
      };

      simulation.ValidateRender = ( int width, int height ) =>
      {
        Assert.IsTrue( getWindowParam );
        Assert.IsFalse( render );
        render = true;

        throw new SuccessException();
      };

      interop.ValidateTerminate = () =>
      {
        Assert.IsTrue( render );
        Assert.IsFalse( terminate );
        terminate = true;
      };

      try
      {
        platform.Run();
      }
      finally
      {
        Assert.IsTrue( terminate );
      }
    }

    [Test]
    [ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "ISettings.ScreenWidth should never be less than one")]
    public void ScreenWidthLessThanOneThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 0;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      platform.Run();
    }

    [Test]
    [ExpectedException( typeof( InvalidOperationException ), ExpectedMessage = "ISettings.ScreenHeight should never be less than one" )]
    public void ScreenHeightLessThanOneThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "Test Window Title";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 0;
      };

      platform.Run();
    }

    [Test]
    [ExpectedException( typeof( InvalidOperationException ), ExpectedMessage = "ISimulation.WindowTitle should never be null or whitespace" )]
    public void WindowTitleNullThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return null;
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      platform.Run();
    }

    [Test]
    [ExpectedException( typeof( InvalidOperationException ), ExpectedMessage = "ISimulation.WindowTitle should never be null or whitespace" )]
    public void WindowTitleEmptyThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return "";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      platform.Run();
    }

    [Test]
    [ExpectedException( typeof( InvalidOperationException ), ExpectedMessage = "ISimulation.WindowTitle should never be null or whitespace" )]
    public void WindowTitleWhitespaceThrowsException()
    {
      var interop = new InteropMock();
      var settings = new SettingsMock();
      var simulation = new SimulationMock();

      var platform = new Platform( interop, settings, simulation ) as IPlatform;

      var windowTitleGet = false;
      var fullscreenGet = false;
      var screenWidthGet = false;
      var screenHeightGet = false;

      simulation.ValidateWindowTitleGet = () =>
      {
        Assert.IsFalse( windowTitleGet );
        windowTitleGet = true;
        return " ";
      };

      settings.ValidateFullscreenGet = () =>
      {
        Assert.IsFalse( fullscreenGet );
        fullscreenGet = true;
        return false;
      };

      settings.ValidateScreenWidthGet = () =>
      {
        Assert.IsFalse( screenWidthGet );
        screenWidthGet = true;
        return 640;
      };

      settings.ValidateScreenHeightGet = () =>
      {
        Assert.IsFalse( screenHeightGet );
        screenHeightGet = true;
        return 480;
      };

      platform.Run();
    }
  }
}